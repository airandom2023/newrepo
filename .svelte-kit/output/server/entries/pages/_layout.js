const ssr = false;
const trailingSlash = "ignore";
export {
  ssr,
  trailingSlash
};
