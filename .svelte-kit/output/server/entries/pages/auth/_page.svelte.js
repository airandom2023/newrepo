import { c as create_ssr_component, a as subscribe, f as escape } from "../../../chunks/ssr.js";
import { u as user, W as WEBUI_NAME } from "../../../chunks/index3.js";
import "../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import "@formatjs/intl-segmenter/polyfill.js";
const Typewriter_svelte_svelte_type_style_lang = "";
const _page_svelte_svelte_type_style_lang = "";
const css = {
  code: ".font-mona.svelte-4luz7t{font-family:'Mona Sans'}.blur-background.svelte-4luz7t{position:fixed;top:0;left:0;width:100%;height:100%;background-size:cover;background-position:center;background-attachment:fixed;background-color:rgba(255, 255, 255, 0.7);z-index:-1}.footer.svelte-4luz7t{text-align:center;padding:20px;bottom:0;width:100%}",
  map: null
};
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_user;
  let $WEBUI_NAME, $$unsubscribe_WEBUI_NAME;
  $$unsubscribe_user = subscribe(user, (value) => value);
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => $WEBUI_NAME = value);
  $$result.css.add(css);
  $$unsubscribe_user();
  $$unsubscribe_WEBUI_NAME();
  return `${$$result.head += `<!-- HEAD_svelte-sdpf07_START -->${$$result.title = `<title> ${escape(`${$WEBUI_NAME}`)} </title>`, ""}<!-- HEAD_svelte-sdpf07_END -->`, ""} ${``}`;
});
export {
  Page as default
};
