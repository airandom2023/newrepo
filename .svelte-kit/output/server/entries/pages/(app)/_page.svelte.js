import { c as create_ssr_component, a as subscribe, f as escape, v as validate_component, b as add_attribute, l as set_store_value } from "../../../chunks/ssr.js";
import { t as tick, c as createNewChat, g as getChatList, a as generateChatCompletion, s as splitStream, b as copyToClipboard, u as updateChatById, d as generateTitle, e as addTagById, f as getAllChatTags, h as deleteTagById, N as Navbar, M as ModelSelector, i as Messages, j as MessageInput, k as cancelChatCompletion, l as getTagsById } from "../../../chunks/Navbar.js";
import { v4 } from "uuid";
import { a as toast } from "../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import { p as page } from "../../../chunks/stores.js";
import { R as RAG_API_BASE_URL, b as settings, d as chatId, m as models, r as releventContexts, e as documents, c as config, f as modelfiles, W as WEBUI_NAME, g as chats, a as WEBUI_BASE_URL, t as tags } from "../../../chunks/index3.js";
/* empty css                                                            */const queryCollection = async (token, collection_names, query, k = null) => {
  let error = null;
  const res = await fetch(`${RAG_API_BASE_URL}/query/collection`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      authorization: `Bearer ${token}`
    },
    body: JSON.stringify({
      collection_names,
      query,
      k
    })
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).catch((err) => {
    error = err.detail;
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
let processing = "";
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $settings, $$unsubscribe_settings;
  let $chatId, $$unsubscribe_chatId;
  let $models, $$unsubscribe_models;
  let $releventContexts, $$unsubscribe_releventContexts;
  let $documents, $$unsubscribe_documents;
  let $config, $$unsubscribe_config;
  let $page, $$unsubscribe_page;
  let $modelfiles, $$unsubscribe_modelfiles;
  let $WEBUI_NAME, $$unsubscribe_WEBUI_NAME;
  $$unsubscribe_settings = subscribe(settings, (value) => $settings = value);
  $$unsubscribe_chatId = subscribe(chatId, (value) => $chatId = value);
  $$unsubscribe_models = subscribe(models, (value) => $models = value);
  $$unsubscribe_releventContexts = subscribe(releventContexts, (value) => $releventContexts = value);
  $$unsubscribe_documents = subscribe(documents, (value) => $documents = value);
  $$unsubscribe_config = subscribe(config, (value) => $config = value);
  $$unsubscribe_page = subscribe(page, (value) => $page = value);
  $$unsubscribe_modelfiles = subscribe(modelfiles, (value) => $modelfiles = value);
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => $WEBUI_NAME = value);
  let stopResponseFlag = false;
  let autoScroll = true;
  let messagesContainerElement;
  let currentRequestId = null;
  let selectedModels = [""];
  let selectedModelfile = null;
  let selectedModelfiles = {};
  let chat = null;
  let tags$1 = [];
  let title = "";
  let prompt = "";
  let files = [];
  let messages = [];
  let history = { messages: {}, currentId: null };
  const initNewChat = async () => {
    if (currentRequestId !== null) {
      await cancelChatCompletion(localStorage.token, currentRequestId);
      currentRequestId = null;
    }
    window.history.replaceState(history.state, "", `/`);
    await chatId.set("");
    autoScroll = true;
    title = "";
    messages = [];
    history = { messages: {}, currentId: null };
    if ($page.url.searchParams.get("models")) {
      selectedModels = $page.url.searchParams.get("models")?.split(",");
    } else if ($settings?.models) {
      selectedModels = $settings?.models;
    } else if ($config?.default_models) {
      selectedModels = $config?.default_models.split(",");
    } else {
      selectedModels = [""];
    }
    selectedModels = selectedModels.map((modelId2) => $models.map((m) => m.id).includes(modelId2) ? modelId2 : "");
    let _settings = JSON.parse(localStorage.getItem("settings") ?? "{}");
    settings.set({ ..._settings });
    const chatInput = document.getElementById("chat-textarea");
    setTimeout(() => chatInput?.focus(), 0);
  };
  const scrollToBottom = () => {
    messagesContainerElement.scrollTop = messagesContainerElement.scrollHeight;
  };
  const submitPrompt = async (userPrompt, _user = null) => {
    console.log("submitPrompt", $chatId);
    selectedModels = selectedModels.map((modelId2) => $models.map((m) => m.id).includes(modelId2) ? modelId2 : "");
    console.log("selectedModels: ", selectedModels);
    let prefix = "";
    if (selectedModels.includes("meteorology:latest")) {
      prefix = "MET_";
    } else if (selectedModels.includes("iap_3903:latest")) {
      prefix = "IAP-3903_";
    } else if (selectedModels.includes("c130j:latest")) {
      prefix = "C130J_";
    } else {
      prefix = "";
    }
    console.log("prefix: ", prefix);
    if (selectedModels.includes("")) {
      toast.error("Model not selected");
    } else if (messages.length != 0 && messages.at(-1).done != true) {
      console.log("wait");
    } else if (files.length > 0 && files.filter((file) => file.upload_status === false).length > 0) {
      toast.error(`Oops! Hold tight! Your files are still in the processing oven. We're cooking them up to perfection. Please be patient and we'll let you know once they're ready.`);
    } else {
      document.getElementById("chat-textarea").style.height = "";
      let filteredCollections = [];
      console.log("$documents: ", $documents);
      filteredCollections = $documents.filter((collection) => collection.collection_name.startsWith(prefix));
      files = [
        ...filteredCollections.length > 0 ? [
          {
            name: prefix + "Documents",
            type: "collection",
            title: prefix + "documents",
            collection_names: filteredCollections.map((doc) => doc.collection_name)
          }
        ] : [],
        ...filteredCollections.reduce(
          (a, e, i, arr) => {
            return [
              .../* @__PURE__ */ new Set([...a, ...(e?.content?.tags ?? []).map((tag) => tag.name)])
            ];
          },
          []
        ).map((tag) => ({
          name: tag,
          type: "collection",
          collection_names: filteredCollections.filter((doc) => (doc?.content?.tags ?? []).map((tag2) => tag2.name).includes(tag)).map((doc) => doc.collection_name)
        }))
      ];
      console.log("filtered_files: ", files);
      let contextObject = await Promise.all(files.map(async (doc) => {
        console.log("Getting Relevent Contexts_doc:", doc);
        return await queryCollection(localStorage.token, doc.collection_names, userPrompt, 4).catch((error) => {
          console.log(error);
          return null;
        });
      }));
      contextObject = contextObject.filter((context) => context);
      console.log("contextObject: ", contextObject);
      console.log("object Keys", Object.keys(contextObject[0]));
      let data = contextObject[0];
      const result = [];
      for (let i = 0; i < data["ids"][0].length; i++) {
        const obj = {};
        Object.keys(data).forEach((key) => {
          if (data[key] !== null && data[key][0][i] !== null && data[key][0][i] !== void 0) {
            obj[key] = data[key][0][i];
          } else {
            obj[key] = null;
          }
        });
        result.push(obj);
      }
      set_store_value(releventContexts, $releventContexts = result.filter((res) => res), $releventContexts);
      console.log("relevantContexts: ", $releventContexts);
      let userMessageId = v4();
      let userMessage = {
        id: userMessageId,
        parentId: messages.length !== 0 ? messages.at(-1).id : null,
        childrenIds: [],
        role: "user",
        user: _user ?? void 0,
        content: userPrompt,
        files: files.length > 0 ? files : void 0,
        timestamp: Math.floor(Date.now() / 1e3)
      };
      history.messages[userMessageId] = userMessage;
      history.currentId = userMessageId;
      if (messages.length !== 0) {
        history.messages[messages.at(-1).id].childrenIds.push(userMessageId);
      }
      await tick();
      if (messages.length == 1) {
        if ($settings.saveChatHistory ?? true) {
          chat = await createNewChat(localStorage.token, {
            id: $chatId,
            title: "New Chat",
            models: selectedModels,
            system: $settings.system ?? void 0,
            options: { ...$settings.options ?? {} },
            messages,
            history,
            tags: [],
            timestamp: Date.now()
          });
          await chats.set(await getChatList(localStorage.token));
          await chatId.set(chat.id);
        } else {
          await chatId.set("local");
        }
        await tick();
      }
      prompt = "";
      files = [];
      await sendPrompt(userPrompt, userMessageId);
    }
  };
  const sendPrompt = async (prompt2, parentId) => {
    const _chatId = JSON.parse(JSON.stringify($chatId));
    await Promise.all(selectedModels.map(async (modelId2) => {
      const model = $models.filter((m) => m.id === modelId2).at(0);
      if (model) {
        let responseMessageId = v4();
        let responseMessage = {
          parentId,
          id: responseMessageId,
          childrenIds: [],
          role: "assistant",
          content: "",
          model: model.id,
          timestamp: Math.floor(Date.now() / 1e3)
        };
        history.messages[responseMessageId] = responseMessage;
        history.currentId = responseMessageId;
        if (parentId !== null) {
          history.messages[parentId].childrenIds = [...history.messages[parentId].childrenIds, responseMessageId];
        }
        if (model) {
          await sendPromptOllama(model, prompt2, responseMessageId, _chatId);
        }
      } else {
        toast.error(`Model ${modelId2} not found`);
      }
    }));
    await chats.set(await getChatList(localStorage.token));
  };
  const sendPromptOllama = async (model, userPrompt, responseMessageId, _chatId) => {
    model = model.id;
    const responseMessage = history.messages[responseMessageId];
    await tick();
    scrollToBottom();
    const messagesBody = [
      $settings.system ? {
        role: "system",
        content: $settings.system
      } : void 0,
      ...messages
    ].filter((message) => message).map((message, idx, arr) => {
      const baseMessage = {
        role: message.role,
        content: arr.length - 2 !== idx ? message.content : message?.raContent ?? message.content
      };
      const imageUrls = message.files?.filter((file) => file.type === "image").map((file) => file.url.slice(file.url.indexOf(",") + 1));
      if (imageUrls && imageUrls.length > 0) {
        baseMessage.images = imageUrls;
      }
      return baseMessage;
    });
    let lastImageIndex = -1;
    messagesBody.forEach((item, index) => {
      if (item.images) {
        lastImageIndex = index;
      }
    });
    messagesBody.forEach((item, index) => {
      if (index !== lastImageIndex) {
        delete item.images;
      }
    });
    const docs = messages.filter((message) => message?.files ?? null).map((message) => message.files.filter((item) => item.type === "doc" || item.type === "collection")).flat(1);
    const [res, controller] = await generateChatCompletion(localStorage.token, {
      model,
      messages: messagesBody,
      options: { ...$settings.options ?? {} },
      format: $settings.requestFormat ?? void 0,
      keep_alive: $settings.keepAlive ?? void 0,
      docs: docs.length > 0 ? docs : void 0
    });
    if (res && res.ok) {
      console.log("controller", controller);
      const reader = res.body.pipeThrough(new TextDecoderStream()).pipeThrough(splitStream("\n")).getReader();
      while (true) {
        const { value, done } = await reader.read();
        if (done || stopResponseFlag || _chatId !== $chatId) {
          responseMessage.done = true;
          messages = messages;
          if (stopResponseFlag) {
            controller.abort("User: Stop Response");
            await cancelChatCompletion(localStorage.token, currentRequestId);
          }
          currentRequestId = null;
          break;
        }
        try {
          let lines = value.split("\n");
          for (const line of lines) {
            if (line !== "") {
              console.log(line);
              let data = JSON.parse(line);
              if ("detail" in data) {
                throw data;
              }
              if ("id" in data) {
                console.log(data);
                currentRequestId = data.id;
              } else {
                if (data.done == false) {
                  if (responseMessage.content == "" && data.message.content == "\n") {
                    continue;
                  } else {
                    responseMessage.content += data.message.content;
                    messages = messages;
                  }
                } else {
                  responseMessage.done = true;
                  if (responseMessage.content == "") {
                    responseMessage.error = true;
                    responseMessage.content = "Oops! No text generated from VayuGPT Server, Please try again.";
                  }
                  responseMessage.context = data.context ?? null;
                  responseMessage.info = {
                    total_duration: data.total_duration,
                    load_duration: data.load_duration,
                    sample_count: data.sample_count,
                    sample_duration: data.sample_duration,
                    prompt_eval_count: data.prompt_eval_count,
                    prompt_eval_duration: data.prompt_eval_duration,
                    eval_count: data.eval_count,
                    eval_duration: data.eval_duration
                  };
                  messages = messages;
                  if ($settings.notificationEnabled && !document.hasFocus()) {
                    const notification = new Notification(
                      selectedModelfile ? `${selectedModelfile.title.charAt(0).toUpperCase() + selectedModelfile.title.slice(1)}` : `${model}`,
                      {
                        body: responseMessage.content,
                        icon: selectedModelfile?.imageUrl ?? `${WEBUI_BASE_URL}/static/favicon.png`
                      }
                    );
                  }
                  if ($settings.responseAutoCopy) {
                    copyToClipboard(responseMessage.content);
                  }
                  if ($settings.responseAutoPlayback) {
                    await tick();
                    document.getElementById(`speak-button-${responseMessage.id}`)?.click();
                  }
                }
              }
            }
          }
        } catch (error) {
          console.log(error);
          if ("detail" in error) {
            toast.error(error.detail);
          }
          break;
        }
        if (autoScroll) {
          scrollToBottom();
        }
      }
      if ($chatId == _chatId) {
        if ($settings.saveChatHistory ?? true) {
          chat = await updateChatById(localStorage.token, _chatId, { messages, history });
          await chats.set(await getChatList(localStorage.token));
        }
      }
    } else {
      if (res !== null) {
        const error = await res.json();
        console.log(error);
        if ("detail" in error) {
          toast.error(error.detail);
          responseMessage.content = error.detail;
        } else {
          toast.error(error.error);
          responseMessage.content = error.error;
        }
      } else {
        toast.error(`Uh-oh! There was an issue connecting to VayuGPT Server.`);
        responseMessage.content = `Uh-oh! There was an issue connecting to VayuGPT Server.`;
      }
      responseMessage.error = true;
      responseMessage.content = `Uh-oh! There was an issue connecting to VayuGPT Server.`;
      responseMessage.done = true;
      messages = messages;
    }
    stopResponseFlag = false;
    await tick();
    if (autoScroll) {
      scrollToBottom();
    }
    if (messages.length == 2 && messages.at(1).content !== "") {
      window.history.replaceState(history.state, "", `/c/${_chatId}`);
      await generateChatTitle(_chatId, userPrompt);
    }
  };
  const stopResponse = () => {
    stopResponseFlag = true;
    console.log("stopResponse");
  };
  const regenerateResponse = async () => {
    console.log("regenerateResponse");
    if (messages.length != 0 && messages.at(-1).done == true) {
      messages.splice(messages.length - 1, 1);
      messages = messages;
      let userMessage = messages.at(-1);
      let userPrompt = userMessage.content;
      await sendPrompt(userPrompt, userMessage.id);
    }
  };
  const continueGeneration = async () => {
    console.log("continueGeneration");
    const _chatId = JSON.parse(JSON.stringify($chatId));
    if (messages.length != 0 && messages.at(-1).done == true) {
      const responseMessage = history.messages[history.currentId];
      responseMessage.done = false;
      await tick();
      const model = $models.filter((m) => m.id === responseMessage.model).at(0);
      if (model) {
        await sendPromptOllama(model, history.messages[responseMessage.parentId].content, responseMessage.id, _chatId);
      }
    } else {
      toast.error(`Model ${modelId} not found`);
    }
  };
  const generateChatTitle = async (_chatId, userPrompt) => {
    if ($settings.titleAutoGenerate ?? true) {
      const title2 = await generateTitle(localStorage.token, $settings?.titleGenerationPrompt ?? "Create a concise, 3-5 word phrase as a header for the following query, strictly adhering to the 3-5 word limit and avoiding the use of the word 'title': {{prompt}}", $settings?.titleAutoGenerateModel ?? selectedModels[0], userPrompt);
      if (title2) {
        await setChatTitle(_chatId, title2);
      }
    } else {
      await setChatTitle(_chatId, `${userPrompt}`);
    }
  };
  const getTags = async () => {
    return await getTagsById(localStorage.token, $chatId).catch(async (error) => {
      return [];
    });
  };
  const addTag = async (tagName) => {
    await addTagById(localStorage.token, $chatId, tagName);
    tags$1 = await getTags();
    chat = await updateChatById(localStorage.token, $chatId, { tags: tags$1 });
    tags.set(await getAllChatTags(localStorage.token));
  };
  const deleteTag = async (tagName) => {
    await deleteTagById(localStorage.token, $chatId, tagName);
    tags$1 = await getTags();
    chat = await updateChatById(localStorage.token, $chatId, { tags: tags$1 });
    tags.set(await getAllChatTags(localStorage.token));
  };
  const setChatTitle = async (_chatId, _title) => {
    if (_chatId === $chatId) {
      title = _title;
    }
    if ($settings.saveChatHistory ?? true) {
      chat = await updateChatById(localStorage.token, _chatId, { title: _title });
      await chats.set(await getChatList(localStorage.token));
    }
  };
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    selectedModelfile = selectedModels.length === 1 && $modelfiles.filter((modelfile) => modelfile.tagName === selectedModels[0]).length > 0 ? $modelfiles.filter((modelfile) => modelfile.tagName === selectedModels[0])[0] : null;
    selectedModelfiles = selectedModels.reduce(
      (a, tagName, i, arr) => {
        const modelfile = $modelfiles.filter((modelfile2) => modelfile2.tagName === tagName)?.at(0) ?? void 0;
        return {
          ...a,
          ...modelfile && { [tagName]: modelfile }
        };
      },
      {}
    );
    {
      if (history.currentId !== null) {
        let _messages = [];
        let currentMessage = history.messages[history.currentId];
        while (currentMessage !== null) {
          _messages.unshift({ ...currentMessage });
          currentMessage = currentMessage.parentId !== null ? history.messages[currentMessage.parentId] : null;
        }
        messages = _messages;
      } else {
        messages = [];
      }
    }
    $$rendered = `${$$result.head += `<!-- HEAD_svelte-1123lr8_START -->${$$result.title = `<title> ${escape(title ? `${title.length > 30 ? `${title.slice(0, 30)}...` : title} | ${$WEBUI_NAME}` : `${$WEBUI_NAME}`)} </title>`, ""}<!-- HEAD_svelte-1123lr8_END -->`, ""}  <div class="h-screen max-h-[100dvh] w-full flex flex-col">${validate_component(Navbar, "Navbar").$$render(
      $$result,
      {
        title,
        shareEnabled: messages.length > 0,
        initNewChat,
        tags: tags$1,
        addTag,
        deleteTag
      },
      {},
      {}
    )} <div class="flex flex-col flex-auto"><div class="pb-2.5 flex flex-col justify-between w-full flex-auto overflow-auto h-0" id="messages-container"${add_attribute("this", messagesContainerElement, 0)}><div class="${escape(
      $settings?.fullScreenMode ?? null ? "max-w-full" : "max-w-2xl md:px-0",
      true
    ) + " mx-auto w-full px-4"}">${validate_component(ModelSelector, "ModelSelector").$$render(
      $$result,
      { selectedModels },
      {
        selectedModels: ($$value) => {
          selectedModels = $$value;
          $$settled = false;
        }
      },
      {}
    )}</div> <div class="h-full w-full flex flex-col py-8">${validate_component(Messages, "Messages").$$render(
      $$result,
      {
        chatId: $chatId,
        selectedModels,
        selectedModelfiles,
        processing,
        bottomPadding: files.length > 0,
        sendPrompt,
        continueGeneration,
        regenerateResponse,
        history,
        messages,
        autoScroll
      },
      {
        history: ($$value) => {
          history = $$value;
          $$settled = false;
        },
        messages: ($$value) => {
          messages = $$value;
          $$settled = false;
        },
        autoScroll: ($$value) => {
          autoScroll = $$value;
          $$settled = false;
        }
      },
      {}
    )}</div></div> ${validate_component(MessageInput, "MessageInput").$$render(
      $$result,
      {
        suggestionPrompts: selectedModelfile?.suggestionPrompts ?? $config.default_prompt_suggestions,
        messages,
        submitPrompt,
        stopResponse,
        files,
        prompt,
        autoScroll,
        selectedModels
      },
      {
        files: ($$value) => {
          files = $$value;
          $$settled = false;
        },
        prompt: ($$value) => {
          prompt = $$value;
          $$settled = false;
        },
        autoScroll: ($$value) => {
          autoScroll = $$value;
          $$settled = false;
        },
        selectedModels: ($$value) => {
          selectedModels = $$value;
          $$settled = false;
        }
      },
      {}
    )}</div></div>`;
  } while (!$$settled);
  $$unsubscribe_settings();
  $$unsubscribe_chatId();
  $$unsubscribe_models();
  $$unsubscribe_releventContexts();
  $$unsubscribe_documents();
  $$unsubscribe_config();
  $$unsubscribe_page();
  $$unsubscribe_modelfiles();
  $$unsubscribe_WEBUI_NAME();
  return $$rendered;
});
export {
  Page as default
};
