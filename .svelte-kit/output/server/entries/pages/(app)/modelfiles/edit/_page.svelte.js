import { c as create_ssr_component, a as subscribe, f as escape, b as add_attribute, g as each } from "../../../../../chunks/ssr.js";
import "../../../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import { p as page } from "../../../../../chunks/stores.js";
import { f as modelfiles } from "../../../../../chunks/index3.js";
import "js-sha256";
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_modelfiles;
  let $$unsubscribe_page;
  $$unsubscribe_modelfiles = subscribe(modelfiles, (value) => value);
  $$unsubscribe_page = subscribe(page, (value) => value);
  let title = "";
  let tagName = "";
  let desc = "";
  let suggestions = [{ content: "" }];
  let categories = {
    character: false,
    assistant: false,
    writing: false,
    productivity: false,
    programming: false,
    "data analysis": false,
    lifestyle: false,
    education: false,
    business: false
  };
  $$unsubscribe_modelfiles();
  $$unsubscribe_page();
  return `<div class="min-h-screen max-h-[100dvh] w-full flex justify-center dark:text-white"><div class="flex flex-col justify-between w-full overflow-y-auto"><div class="max-w-2xl mx-auto w-full px-3 md:px-0 my-10"><input type="file" hidden accept="image/*"> <div class="text-2xl font-semibold mb-6" data-svelte-h="svelte-14jlof8">My Modelfiles</div> <button class="flex space-x-1" data-svelte-h="svelte-undlmf"><div class="self-center"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M17 10a.75.75 0 01-.75.75H5.612l4.158 3.96a.75.75 0 11-1.04 1.08l-5.5-5.25a.75.75 0 010-1.08l5.5-5.25a.75.75 0 111.04 1.08L5.612 9.25H16.25A.75.75 0 0117 10z" clip-rule="evenodd"></path></svg></div> <div class="self-center font-medium text-sm">Back</div></button> <hr class="my-3 dark:border-gray-700"> <form class="flex flex-col"><div class="flex justify-center my-4"><div class="self-center"><button class="${"" + escape("p-6", true) + " rounded-full dark:bg-gray-700 border border-dashed border-gray-200"}" type="button">${`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-8"><path fill-rule="evenodd" d="M12 3.75a.75.75 0 01.75.75v6.75h6.75a.75.75 0 010 1.5h-6.75v6.75a.75.75 0 01-1.5 0v-6.75H4.5a.75.75 0 010-1.5h6.75V4.5a.75.75 0 01.75-.75z" clip-rule="evenodd"></path></svg>`}</button></div></div> <div class="my-2 flex space-x-2"><div class="flex-1"><div class="text-sm font-semibold mb-2" data-svelte-h="svelte-7d74a3">Name*</div> <div><input class="px-3 py-1.5 text-sm w-full bg-transparent border dark:border-gray-600 outline-none rounded-lg" placeholder="Name your modelfile" required${add_attribute("value", title, 0)}></div></div> <div class="flex-1"><div class="text-sm font-semibold mb-2" data-svelte-h="svelte-adac66">Model Tag Name*</div> <div><input class="px-3 py-1.5 text-sm w-full bg-transparent disabled:text-gray-500 border dark:border-gray-600 outline-none rounded-lg" placeholder="Add a model tag name"${add_attribute("value", tagName, 0)} disabled required></div></div></div> <div class="my-2"><div class="text-sm font-semibold mb-2" data-svelte-h="svelte-17hu182">Description*</div> <div><input class="px-3 py-1.5 text-sm w-full bg-transparent border dark:border-gray-600 outline-none rounded-lg" placeholder="Add a short description about what this modelfile does" required${add_attribute("value", desc, 0)}></div></div> <div class="my-2"><div class="flex w-full justify-between" data-svelte-h="svelte-oybxeh"><div class="self-center text-sm font-semibold">Modelfile</div></div>  <div class="mt-2"><div class="text-xs font-semibold mb-2" data-svelte-h="svelte-2imxe0">Content*</div> <div><textarea class="px-3 py-1.5 text-sm w-full bg-transparent border dark:border-gray-600 outline-none rounded-lg"${add_attribute("placeholder", `FROM llama2
PARAMETER temperature 1
SYSTEM """
You are Mario from Super Mario Bros, acting as an assistant.
"""`, 0)} rows="6" required>${escape("")}</textarea></div></div></div> <div class="my-2"><div class="flex w-full justify-between mb-2"><div class="self-center text-sm font-semibold" data-svelte-h="svelte-cktctz">Prompt suggestions</div> <button class="p-1 px-3 text-xs flex rounded transition" type="button" data-svelte-h="svelte-5pubto"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path d="M10.75 4.75a.75.75 0 00-1.5 0v4.5h-4.5a.75.75 0 000 1.5h4.5v4.5a.75.75 0 001.5 0v-4.5h4.5a.75.75 0 000-1.5h-4.5v-4.5z"></path></svg></button></div> <div class="flex flex-col space-y-1">${each(suggestions, (prompt, promptIdx) => {
    return `<div class="flex border dark:border-gray-600 rounded-lg"><input class="px-3 py-1.5 text-sm w-full bg-transparent outline-none border-r dark:border-gray-600" placeholder="Write a prompt suggestion (e.g. Who are you?)"${add_attribute("value", prompt.content, 0)}> <button class="px-2" type="button" data-svelte-h="svelte-1tpj1wu"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z"></path></svg></button> </div>`;
  })}</div></div> <div class="my-2"><div class="text-sm font-semibold mb-2" data-svelte-h="svelte-1w33zjy">Categories</div> <div class="grid grid-cols-4">${each(Object.keys(categories), (category) => {
    return `<div class="flex space-x-2 text-sm"><input type="checkbox"${add_attribute("checked", categories[category], 1)}> <div class="capitalize">${escape(category)}</div> </div>`;
  })}</div></div> ${``} <div class="my-2 flex justify-end"><button class="${"text-sm px-3 py-2 transition rounded-xl " + escape(
    " bg-gray-50 hover:bg-gray-100 dark:bg-gray-700 dark:hover:bg-gray-800",
    true
  ) + " flex"}" type="submit" ${""}><div class="self-center font-medium" data-svelte-h="svelte-5s0nx6">Save &amp; Update</div> ${``}</button></div></form></div></div></div>`;
});
export {
  Page as default
};
