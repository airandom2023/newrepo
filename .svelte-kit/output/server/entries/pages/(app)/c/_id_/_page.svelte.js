import { c as create_ssr_component, a as subscribe, f as escape, v as validate_component, b as add_attribute } from "../../../../../chunks/ssr.js";
import { t as tick, N as Navbar, M as ModelSelector, i as Messages, j as MessageInput, m as getChatById, n as convertMessagesToHistory, c as createNewChat, g as getChatList, a as generateChatCompletion, s as splitStream, b as copyToClipboard, u as updateChatById, d as generateTitle, l as getTagsById, e as addTagById, f as getAllChatTags, h as deleteTagById, k as cancelChatCompletion } from "../../../../../chunks/Navbar.js";
import { v4 } from "uuid";
import { a as toast } from "../../../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import { p as page } from "../../../../../chunks/stores.js";
import { b as settings, d as chatId, m as models, f as modelfiles, W as WEBUI_NAME, c as config, g as chats, a as WEBUI_BASE_URL, t as tags } from "../../../../../chunks/index3.js";
/* empty css                                                                  */function client_method(key) {
  {
    if (key === "before_navigate" || key === "after_navigate" || key === "on_navigate") {
      return () => {
      };
    } else {
      const name_lookup = {
        disable_scroll_handling: "disableScrollHandling",
        preload_data: "preloadData",
        preload_code: "preloadCode",
        invalidate_all: "invalidateAll"
      };
      return () => {
        throw new Error(`Cannot call ${name_lookup[key] ?? key}(...) on the server`);
      };
    }
  }
}
const goto = /* @__PURE__ */ client_method("goto");
const css = {
  code: ".blur-background.svelte-1ldge4x{position:fixed;top:0;left:0;width:100%;height:100%;background-size:cover;background-position:center;background-attachment:fixed;background-color:rgba(255, 255, 255, 0.7);filter:blur(4px);z-index:-1}",
  map: null
};
const BlurBackground = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  $$result.css.add(css);
  return `<div class="blur-background svelte-1ldge4x" data-svelte-h="svelte-1bnt34n"><img src="bg_1.webp" alt=""> </div>`;
});
let processing = "";
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $settings, $$unsubscribe_settings;
  let $chatId, $$unsubscribe_chatId;
  let $models, $$unsubscribe_models;
  let $page, $$unsubscribe_page;
  let $modelfiles, $$unsubscribe_modelfiles;
  let $WEBUI_NAME, $$unsubscribe_WEBUI_NAME;
  let $config, $$unsubscribe_config;
  $$unsubscribe_settings = subscribe(settings, (value) => $settings = value);
  $$unsubscribe_chatId = subscribe(chatId, (value) => $chatId = value);
  $$unsubscribe_models = subscribe(models, (value) => $models = value);
  $$unsubscribe_page = subscribe(page, (value) => $page = value);
  $$unsubscribe_modelfiles = subscribe(modelfiles, (value) => $modelfiles = value);
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => $WEBUI_NAME = value);
  $$unsubscribe_config = subscribe(config, (value) => $config = value);
  let loaded = false;
  let stopResponseFlag = false;
  let autoScroll = true;
  let messagesContainerElement;
  let currentRequestId = null;
  let selectedModels = [""];
  let selectedModelfile = null;
  let selectedModelfiles = {};
  let chat = null;
  let tags$1 = [];
  let title = "";
  let prompt = "";
  let files = [];
  let messages = [];
  let history = { messages: {}, currentId: null };
  const loadChat = async () => {
    await chatId.set($page.params.id);
    chat = await getChatById(localStorage.token, $chatId).catch(async (error) => {
      await goto("/");
      return null;
    });
    if (chat) {
      tags$1 = await getTags();
      const chatContent = chat.chat;
      if (chatContent) {
        console.log(chatContent);
        selectedModels = (chatContent?.models ?? void 0) !== void 0 ? chatContent.models : [chatContent.models ?? ""];
        history = (chatContent?.history ?? void 0) !== void 0 ? chatContent.history : convertMessagesToHistory(chatContent.messages);
        title = chatContent.title;
        let _settings = JSON.parse(localStorage.getItem("settings") ?? "{}");
        await settings.set({
          ..._settings,
          system: chatContent.system ?? _settings.system,
          options: chatContent.options ?? _settings.options
        });
        autoScroll = true;
        await tick();
        if (messages.length > 0) {
          history.messages[messages.at(-1).id].done = true;
        }
        await tick();
        return true;
      } else {
        return null;
      }
    }
  };
  const scrollToBottom = () => {
    messagesContainerElement.scrollTop = messagesContainerElement.scrollHeight;
  };
  const submitPrompt = async (userPrompt, _user = null) => {
    console.log("submitPrompt", $chatId);
    if (selectedModels.includes("")) {
      toast.error("Model not selected");
    } else if (messages.length != 0 && messages.at(-1).done != true) {
      console.log("wait");
    } else if (files.length > 0 && files.filter((file) => file.upload_status === false).length > 0) {
      toast.error(`Oops! Hold tight! Your files are still in the processing oven. We're cooking them up to perfection. Please be patient and we'll let you know once they're ready.`);
    } else {
      document.getElementById("chat-textarea").style.height = "";
      let userMessageId = v4();
      let userMessage = {
        id: userMessageId,
        parentId: messages.length !== 0 ? messages.at(-1).id : null,
        childrenIds: [],
        role: "user",
        user: _user ?? void 0,
        content: userPrompt,
        files: files.length > 0 ? files : void 0,
        timestamp: Math.floor(Date.now() / 1e3)
      };
      history.messages[userMessageId] = userMessage;
      history.currentId = userMessageId;
      if (messages.length !== 0) {
        history.messages[messages.at(-1).id].childrenIds.push(userMessageId);
      }
      await tick();
      if (messages.length == 1) {
        if ($settings.saveChatHistory ?? true) {
          chat = await createNewChat(localStorage.token, {
            id: $chatId,
            title: "New Chat",
            models: selectedModels,
            system: $settings.system ?? void 0,
            options: { ...$settings.options ?? {} },
            messages,
            history,
            timestamp: Date.now()
          });
          await chats.set(await getChatList(localStorage.token));
          await chatId.set(chat.id);
        } else {
          await chatId.set("local");
        }
        await tick();
      }
      prompt = "";
      files = [];
      await sendPrompt(userPrompt, userMessageId);
    }
  };
  const sendPrompt = async (prompt2, parentId) => {
    const _chatId = JSON.parse(JSON.stringify($chatId));
    await Promise.all(selectedModels.map(async (modelId2) => {
      const model = $models.filter((m) => m.id === modelId2).at(0);
      if (model) {
        let responseMessageId = v4();
        let responseMessage = {
          parentId,
          id: responseMessageId,
          childrenIds: [],
          role: "assistant",
          content: "",
          model: model.id,
          timestamp: Math.floor(Date.now() / 1e3)
        };
        history.messages[responseMessageId] = responseMessage;
        history.currentId = responseMessageId;
        if (parentId !== null) {
          history.messages[parentId].childrenIds = [...history.messages[parentId].childrenIds, responseMessageId];
        }
        if (model) {
          await sendPromptOllama(model, prompt2, responseMessageId, _chatId);
        }
      } else {
        toast.error(`Model ${modelId2} not found`);
      }
    }));
    await chats.set(await getChatList(localStorage.token));
  };
  const sendPromptOllama = async (model, userPrompt, responseMessageId, _chatId) => {
    model = model.id;
    const responseMessage = history.messages[responseMessageId];
    await tick();
    scrollToBottom();
    const messagesBody = [
      $settings.system ? {
        role: "system",
        content: $settings.system
      } : void 0,
      ...messages
    ].filter((message) => message).map((message, idx, arr) => {
      const baseMessage = {
        role: message.role,
        content: arr.length - 2 !== idx ? message.content : message?.raContent ?? message.content
      };
      const imageUrls = message.files?.filter((file) => file.type === "image").map((file) => file.url.slice(file.url.indexOf(",") + 1));
      if (imageUrls && imageUrls.length > 0) {
        baseMessage.images = imageUrls;
      }
      return baseMessage;
    });
    let lastImageIndex = -1;
    messagesBody.forEach((item, index) => {
      if (item.images) {
        lastImageIndex = index;
      }
    });
    messagesBody.forEach((item, index) => {
      if (index !== lastImageIndex) {
        delete item.images;
      }
    });
    const docs = messages.filter((message) => message?.files ?? null).map((message) => message.files.filter((item) => item.type === "doc" || item.type === "collection")).flat(1);
    const [res, controller] = await generateChatCompletion(localStorage.token, {
      model,
      messages: messagesBody,
      options: { ...$settings.options ?? {} },
      format: $settings.requestFormat ?? void 0,
      keep_alive: $settings.keepAlive ?? void 0,
      docs: docs.length > 0 ? docs : void 0
    });
    if (res && res.ok) {
      console.log("controller", controller);
      const reader = res.body.pipeThrough(new TextDecoderStream()).pipeThrough(splitStream("\n")).getReader();
      while (true) {
        const { value, done } = await reader.read();
        if (done || stopResponseFlag || _chatId !== $chatId) {
          responseMessage.done = true;
          messages = messages;
          if (stopResponseFlag) {
            controller.abort("User: Stop Response");
            await cancelChatCompletion(localStorage.token, currentRequestId);
          }
          currentRequestId = null;
          break;
        }
        try {
          let lines = value.split("\n");
          for (const line of lines) {
            if (line !== "") {
              console.log(line);
              let data = JSON.parse(line);
              if ("detail" in data) {
                throw data;
              }
              if ("id" in data) {
                console.log(data);
                currentRequestId = data.id;
              } else {
                if (data.done == false) {
                  if (responseMessage.content == "" && data.message.content == "\n") {
                    continue;
                  } else {
                    responseMessage.content += data.message.content;
                    messages = messages;
                  }
                } else {
                  responseMessage.done = true;
                  if (responseMessage.content == "") {
                    responseMessage.error = true;
                    responseMessage.content = "Oops! No text generated from Ollama, Please try again.";
                  }
                  responseMessage.context = data.context ?? null;
                  responseMessage.info = {
                    total_duration: data.total_duration,
                    load_duration: data.load_duration,
                    sample_count: data.sample_count,
                    sample_duration: data.sample_duration,
                    prompt_eval_count: data.prompt_eval_count,
                    prompt_eval_duration: data.prompt_eval_duration,
                    eval_count: data.eval_count,
                    eval_duration: data.eval_duration
                  };
                  messages = messages;
                  if ($settings.notificationEnabled && !document.hasFocus()) {
                    const notification = new Notification(
                      selectedModelfile ? `${selectedModelfile.title.charAt(0).toUpperCase() + selectedModelfile.title.slice(1)}` : `${model}`,
                      {
                        body: responseMessage.content,
                        icon: selectedModelfile?.imageUrl ?? `${WEBUI_BASE_URL}/static/favicon.png`
                      }
                    );
                  }
                  if ($settings.responseAutoCopy) {
                    copyToClipboard(responseMessage.content);
                  }
                  if ($settings.responseAutoPlayback) {
                    await tick();
                    document.getElementById(`speak-button-${responseMessage.id}`)?.click();
                  }
                }
              }
            }
          }
        } catch (error) {
          console.log(error);
          if ("detail" in error) {
            toast.error(error.detail);
          }
          break;
        }
        if (autoScroll) {
          scrollToBottom();
        }
      }
      if ($chatId == _chatId) {
        if ($settings.saveChatHistory ?? true) {
          chat = await updateChatById(localStorage.token, _chatId, { messages, history });
          await chats.set(await getChatList(localStorage.token));
        }
      }
    } else {
      if (res !== null) {
        const error = await res.json();
        console.log(error);
        if ("detail" in error) {
          toast.error(error.detail);
          responseMessage.content = error.detail;
        } else {
          toast.error(error.error);
          responseMessage.content = error.error;
        }
      } else {
        toast.error(`Uh-oh! There was an issue connecting to Ollama.`);
        responseMessage.content = `Uh-oh! There was an issue connecting to Ollama.`;
      }
      responseMessage.error = true;
      responseMessage.content = `Uh-oh! There was an issue connecting to Ollama.`;
      responseMessage.done = true;
      messages = messages;
    }
    stopResponseFlag = false;
    await tick();
    if (autoScroll) {
      scrollToBottom();
    }
    if (messages.length == 2 && messages.at(1).content !== "") {
      window.history.replaceState(history.state, "", `/c/${_chatId}`);
      await generateChatTitle(_chatId, userPrompt);
    }
  };
  const stopResponse = () => {
    stopResponseFlag = true;
    console.log("stopResponse");
  };
  const continueGeneration = async () => {
    console.log("continueGeneration");
    const _chatId = JSON.parse(JSON.stringify($chatId));
    if (messages.length != 0 && messages.at(-1).done == true) {
      const responseMessage = history.messages[history.currentId];
      responseMessage.done = false;
      await tick();
      const model = $models.filter((m) => m.id === responseMessage.model).at(0);
      if (model) {
        await sendPromptOllama(model, history.messages[responseMessage.parentId].content, responseMessage.id, _chatId);
      }
    } else {
      toast.error(`Model ${modelId} not found`);
    }
  };
  const regenerateResponse = async () => {
    console.log("regenerateResponse");
    if (messages.length != 0 && messages.at(-1).done == true) {
      messages.splice(messages.length - 1, 1);
      messages = messages;
      let userMessage = messages.at(-1);
      let userPrompt = userMessage.content;
      await sendPrompt(userPrompt, userMessage.id);
    }
  };
  const generateChatTitle = async (_chatId, userPrompt) => {
    if ($settings.titleAutoGenerate ?? true) {
      const title2 = await generateTitle(localStorage.token, $settings?.titleGenerationPrompt ?? "Create a concise, 3-5 word phrase as a header for the following query, strictly adhering to the 3-5 word limit and avoiding the use of the word 'title': {{prompt}}", $settings?.titleAutoGenerateModel ?? selectedModels[0], userPrompt);
      if (title2) {
        await setChatTitle(_chatId, title2);
      }
    } else {
      await setChatTitle(_chatId, `${userPrompt}`);
    }
  };
  const setChatTitle = async (_chatId, _title) => {
    if (_chatId === $chatId) {
      title = _title;
    }
    chat = await updateChatById(localStorage.token, _chatId, { title: _title });
    await chats.set(await getChatList(localStorage.token));
  };
  const getTags = async () => {
    return await getTagsById(localStorage.token, $chatId).catch(async (error) => {
      return [];
    });
  };
  const addTag = async (tagName) => {
    await addTagById(localStorage.token, $chatId, tagName);
    tags$1 = await getTags();
    chat = await updateChatById(localStorage.token, $chatId, { tags: tags$1 });
    tags.set(await getAllChatTags(localStorage.token));
  };
  const deleteTag = async (tagName) => {
    await deleteTagById(localStorage.token, $chatId, tagName);
    tags$1 = await getTags();
    chat = await updateChatById(localStorage.token, $chatId, { tags: tags$1 });
    tags.set(await getAllChatTags(localStorage.token));
  };
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    selectedModelfile = selectedModels.length === 1 && $modelfiles.filter((modelfile) => modelfile.tagName === selectedModels[0]).length > 0 ? $modelfiles.filter((modelfile) => modelfile.tagName === selectedModels[0])[0] : null;
    selectedModelfiles = selectedModels.reduce(
      (a, tagName, i, arr) => {
        const modelfile = $modelfiles.filter((modelfile2) => modelfile2.tagName === tagName)?.at(0) ?? void 0;
        return {
          ...a,
          ...modelfile && { [tagName]: modelfile }
        };
      },
      {}
    );
    {
      if (history.currentId !== null) {
        let _messages = [];
        let currentMessage = history.messages[history.currentId];
        while (currentMessage !== null) {
          _messages.unshift({ ...currentMessage });
          currentMessage = currentMessage.parentId !== null ? history.messages[currentMessage.parentId] : null;
        }
        messages = _messages;
      } else {
        messages = [];
      }
    }
    {
      if ($page.params.id) {
        (async () => {
          if (await loadChat()) {
            await tick();
            loaded = true;
            window.setTimeout(() => scrollToBottom(), 0);
            const chatInput = document.getElementById("chat-textarea");
            chatInput?.focus();
          } else {
            await goto("/");
          }
        })();
      }
    }
    $$rendered = `${$$result.head += `<!-- HEAD_svelte-1123lr8_START -->${$$result.title = `<title> ${escape(title ? `${title.length > 30 ? `${title.slice(0, 30)}...` : title} | ${$WEBUI_NAME}` : `${$WEBUI_NAME}`)} </title>`, ""}<!-- HEAD_svelte-1123lr8_END -->`, ""} ${loaded ? `<div class="min-h-screen max-h-screen w-full flex flex-col">${validate_component(Navbar, "Navbar").$$render(
      $$result,
      {
        title,
        shareEnabled: messages.length > 0,
        initNewChat: async () => {
          if (currentRequestId !== null) {
            await cancelChatCompletion(localStorage.token, currentRequestId);
            currentRequestId = null;
          }
          goto("/");
        },
        tags: tags$1,
        addTag,
        deleteTag
      },
      {},
      {}
    )} <div class="flex flex-col flex-auto"><div class="pb-2.5 flex flex-col justify-between w-full flex-auto overflow-auto h-0" id="messages-container"${add_attribute("this", messagesContainerElement, 0)}><div class="${escape(
      $settings?.fullScreenMode ?? null ? "max-w-full" : "max-w-2xl md:px-0",
      true
    ) + " mx-auto w-full px-4"}">${validate_component(ModelSelector, "ModelSelector").$$render(
      $$result,
      { selectedModels },
      {
        selectedModels: ($$value) => {
          selectedModels = $$value;
          $$settled = false;
        }
      },
      {}
    )}</div> <div class="h-full w-full flex flex-col py-8">${validate_component(Messages, "Messages").$$render(
      $$result,
      {
        chatId: $chatId,
        selectedModels,
        selectedModelfiles,
        processing,
        bottomPadding: files.length > 0,
        sendPrompt,
        continueGeneration,
        regenerateResponse,
        history,
        messages,
        autoScroll
      },
      {
        history: ($$value) => {
          history = $$value;
          $$settled = false;
        },
        messages: ($$value) => {
          messages = $$value;
          $$settled = false;
        },
        autoScroll: ($$value) => {
          autoScroll = $$value;
          $$settled = false;
        }
      },
      {}
    )}</div></div> ${validate_component(BlurBackground, "BlurBackground").$$render($$result, {}, {}, {})} ${validate_component(MessageInput, "MessageInput").$$render(
      $$result,
      {
        suggestionPrompts: selectedModelfile?.suggestionPrompts ?? $config.default_prompt_suggestions,
        messages,
        submitPrompt,
        stopResponse,
        files,
        prompt,
        autoScroll
      },
      {
        files: ($$value) => {
          files = $$value;
          $$settled = false;
        },
        prompt: ($$value) => {
          prompt = $$value;
          $$settled = false;
        },
        autoScroll: ($$value) => {
          autoScroll = $$value;
          $$settled = false;
        }
      },
      {}
    )}</div></div>` : ``}`;
  } while (!$$settled);
  $$unsubscribe_settings();
  $$unsubscribe_chatId();
  $$unsubscribe_models();
  $$unsubscribe_page();
  $$unsubscribe_modelfiles();
  $$unsubscribe_WEBUI_NAME();
  $$unsubscribe_config();
  return $$rendered;
});
export {
  Page as default
};
