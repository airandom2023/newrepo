import { c as create_ssr_component, g as each, f as escape, b as add_attribute, a as subscribe, v as validate_component } from "../../../../chunks/ssr.js";
import "../../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import { c as config, b as settings, u as user, m as models, W as WEBUI_NAME } from "../../../../chunks/index3.js";
import "js-sha256";
const ChatCompletion = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { messages = [] } = $$props;
  let textAreaElement;
  if ($$props.messages === void 0 && $$bindings.messages && messages !== void 0)
    $$bindings.messages(messages);
  return `<div class="py-3 space-y-3">${each(messages, (message, idx) => {
    return `<div class="flex gap-2 group"><div class="flex items-start pt-1"><button class="px-2 py-1 text-sm font-semibold uppercase min-w-[6rem] text-left dark:group-hover:bg-gray-800 rounded-lg transition">${escape(message.role)}</button></div> <div class="flex-1"><textarea id="${escape(message.role, true) + "-" + escape(idx, true) + "-textarea"}" class="w-full bg-transparent outline-none rounded-lg p-2 text-sm resize-none overflow-hidden" placeholder="${"Enter " + escape(message.role === "user" ? "a user" : "an assistant", true) + " message here"}" rows="1"${add_attribute("this", textAreaElement, 0)}>${escape(message.content || "")}</textarea></div> <div class="pt-1"><button class="group-hover:text-gray-500 dark:text-gray-900 dark:hover:text-gray-300 transition" data-svelte-h="svelte-1526cro"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-5 h-5"><path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"></path></svg></button> </div></div> <hr class="dark:border-gray-800">`;
  })} <button class="flex items-center gap-2 px-2 py-1" data-svelte-h="svelte-1mgq0f0"><div><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5"><path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"></path></svg></div> <div class="text-sm font-medium">Add message</div></button></div>`;
});
const _page_svelte_svelte_type_style_lang = "";
const css = {
  code: ".scrollbar-hidden.svelte-8lkua9::-webkit-scrollbar{display:none}.scrollbar-hidden.svelte-8lkua9{-ms-overflow-style:none;scrollbar-width:none}",
  map: null
};
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_config;
  let $$unsubscribe_settings;
  let $$unsubscribe_user;
  let $models, $$unsubscribe_models;
  let $WEBUI_NAME, $$unsubscribe_WEBUI_NAME;
  $$unsubscribe_config = subscribe(config, (value) => value);
  $$unsubscribe_settings = subscribe(settings, (value) => value);
  $$unsubscribe_user = subscribe(user, (value) => value);
  $$unsubscribe_models = subscribe(models, (value) => $models = value);
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => $WEBUI_NAME = value);
  let mode = "chat";
  let messagesContainerElement;
  let messages = [{ role: "user", content: "" }];
  $$result.css.add(css);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${$$result.head += `<!-- HEAD_svelte-1a6ygk0_START -->${$$result.title = `<title> ${escape(`Playground | ${$WEBUI_NAME}`)} </title>`, ""}<!-- HEAD_svelte-1a6ygk0_END -->`, ""} <div class="min-h-screen max-h-[100dvh] w-full flex justify-center dark:text-white"><div class="flex flex-col justify-between w-full overflow-y-auto h-[100dvh]"><div class="max-w-2xl mx-auto w-full px-3 md:px-0 my-10 h-full"><div class="flex flex-col h-full"><div class="flex flex-col justify-between mb-2.5 gap-1"><div class="flex justify-between items-center gap-2"><div class="text-2xl font-semibold self-center flex" data-svelte-h="svelte-1uunymb">Playground <span class="text-xs text-gray-500 self-center ml-1">(Beta)</span></div> <div><button class="${"flex items-center gap-0.5 text-xs px-2.5 py-0.5 rounded-lg " + escape("text-sky-600 dark:text-sky-200 bg-sky-200/30", true) + " " + escape(mode === "complete", true) + " svelte-8lkua9"}">${`${`Chat`}`} <div data-svelte-h="svelte-1bmoxmc"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-3 h-3"><path fill-rule="evenodd" d="M5.22 10.22a.75.75 0 0 1 1.06 0L8 11.94l1.72-1.72a.75.75 0 1 1 1.06 1.06l-2.25 2.25a.75.75 0 0 1-1.06 0l-2.25-2.25a.75.75 0 0 1 0-1.06ZM10.78 5.78a.75.75 0 0 1-1.06 0L8 4.06 6.28 5.78a.75.75 0 0 1-1.06-1.06l2.25-2.25a.75.75 0 0 1 1.06 0l2.25 2.25a.75.75 0 0 1 0 1.06Z" clip-rule="evenodd"></path></svg></div></button></div></div> <div class="flex gap-1 px-1"><select id="models" class="outline-none bg-transparent text-sm font-medium rounded-lg w-full placeholder-gray-400"><option class="text-gray-800" value="" selected disabled data-svelte-h="svelte-1qxywsw">Select a model</option>${each($models, (model) => {
      return `${model.name === "hr" ? `<hr>` : `<option${add_attribute("value", model.id, 0)} class="text-gray-800 text-lg">${escape(model.name + `${model.size ? ` (${(model.size / 1024 ** 3).toFixed(1)}GB)` : ""}`)}</option>`}`;
    })}</select> </div></div> ${`<div class="p-1"><div class="p-3 outline outline-1 outline-gray-200 dark:outline-gray-800 rounded-lg"><div class="text-sm font-medium" data-svelte-h="svelte-6nxi37">System</div> <textarea id="system-textarea" class="w-full h-full bg-transparent resize-none outline-none text-sm" placeholder="You're a helpful assistant." rows="4">${escape("")}</textarea></div></div>`} <div class="pb-2.5 flex flex-col justify-between w-full flex-auto overflow-auto h-0" id="messages-container"${add_attribute("this", messagesContainerElement, 0)}><div class="h-full w-full flex flex-col"><div class="flex-1 p-1">${`${`${validate_component(ChatCompletion, "ChatCompletion").$$render(
      $$result,
      { messages },
      {
        messages: ($$value) => {
          messages = $$value;
          $$settled = false;
        }
      },
      {}
    )}`}`}</div></div></div> <div class="pb-2">${`<button class="px-3 py-1.5 text-sm font-medium bg-emerald-600 hover:bg-emerald-700 text-gray-50 transition rounded-lg" data-svelte-h="svelte-1h6musc">Submit</button>`}</div></div></div></div> </div>`;
  } while (!$$settled);
  $$unsubscribe_config();
  $$unsubscribe_settings();
  $$unsubscribe_user();
  $$unsubscribe_models();
  $$unsubscribe_WEBUI_NAME();
  return $$rendered;
});
export {
  Page as default
};
