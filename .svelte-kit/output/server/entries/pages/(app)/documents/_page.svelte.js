import { c as create_ssr_component, p as createEventDispatcher, f as escape, v as validate_component, b as add_attribute, a as subscribe, g as each } from "../../../../chunks/ssr.js";
import "../../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import "file-saver";
import { h as WEBUI_API_BASE_URL, e as documents, W as WEBUI_NAME } from "../../../../chunks/index3.js";
import "js-sha256";
import "dayjs";
import { M as Modal } from "../../../../chunks/Modal.js";
import { T as Tags } from "../../../../chunks/Tags.js";
const getDocs = async (token = "") => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/documents/`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      authorization: `Bearer ${token}`
    }
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err.detail;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const tagDocByName = async (token, name, form) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/documents/name/${name}/tags`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      authorization: `Bearer ${token}`
    },
    body: JSON.stringify({
      name: form.name,
      tags: form.tags
    })
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err.detail;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const Checkbox = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  createEventDispatcher();
  let { state = "unchecked" } = $$props;
  let { indeterminate = false } = $$props;
  let _state = "unchecked";
  if ($$props.state === void 0 && $$bindings.state && state !== void 0)
    $$bindings.state(state);
  if ($$props.indeterminate === void 0 && $$bindings.indeterminate && indeterminate !== void 0)
    $$bindings.indeterminate(indeterminate);
  _state = state;
  return `<button class="${"outline -outline-offset-1 outline-[1.5px] outline-gray-200 dark:outline-gray-600 " + escape(
    state !== "unchecked" ? "bg-black outline-black " : "hover:outline-gray-500 hover:bg-gray-50 dark:hover:bg-gray-800",
    true
  ) + " text-white transition-all rounded inline-block w-3.5 h-3.5 relative"}"><div class="top-0 left-0 absolute w-full flex justify-center">${_state === "checked" ? `<svg class="w-3.5 h-3.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" d="m5 12 4.7 4.5 9.3-9"></path></svg>` : `${indeterminate ? `<svg class="w-3 h-3.5 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" d="M5 12h14"></path></svg>` : ``}`}</div> </button>`;
});
const EditDocModal_svelte_svelte_type_style_lang = "";
const css$1 = {
  code: "input.svelte-1vx7r9s::-webkit-outer-spin-button,input.svelte-1vx7r9s::-webkit-inner-spin-button{-webkit-appearance:none;margin:0}",
  map: null
};
const EditDocModal = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { show = false } = $$props;
  let { selectedDoc } = $$props;
  let tags = [];
  let doc = { name: "", title: "", content: null };
  const addTagHandler = async (tagName) => {
    if (!tags.find((tag) => tag.name === tagName) && tagName !== "") {
      tags = [...tags, { name: tagName }];
      await tagDocByName(localStorage.token, doc.name, { name: doc.name, tags });
      documents.set(await getDocs(localStorage.token));
    } else {
      console.log("tag already exists");
    }
  };
  const deleteTagHandler = async (tagName) => {
    tags = tags.filter((tag) => tag.name !== tagName);
    await tagDocByName(localStorage.token, doc.name, { name: doc.name, tags });
    documents.set(await getDocs(localStorage.token));
  };
  if ($$props.show === void 0 && $$bindings.show && show !== void 0)
    $$bindings.show(show);
  if ($$props.selectedDoc === void 0 && $$bindings.selectedDoc && selectedDoc !== void 0)
    $$bindings.selectedDoc(selectedDoc);
  $$result.css.add(css$1);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${validate_component(Modal, "Modal").$$render(
      $$result,
      { size: "sm", show },
      {
        show: ($$value) => {
          show = $$value;
          $$settled = false;
        }
      },
      {
        default: () => {
          return `<div><div class="flex justify-between dark:text-gray-300 px-5 py-4"><div class="text-lg font-medium self-center" data-svelte-h="svelte-1ril91t">Edit Doc</div> <button class="self-center" data-svelte-h="svelte-745w2y"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z"></path></svg></button></div> <hr class="dark:border-gray-800"> <div class="flex flex-col md:flex-row w-full px-5 py-4 md:space-x-4 dark:text-gray-200"><div class="flex flex-col w-full sm:flex-row sm:justify-center sm:space-x-6"><form class="flex flex-col w-full"><div class="flex flex-col space-y-1.5"><div class="flex flex-col w-full"><div class="mb-1 text-xs text-gray-500" data-svelte-h="svelte-11m42gz">Name Tag</div> <div class="flex flex-1"><div class="bg-gray-200 dark:bg-gray-600 font-bold px-3 py-1 border border-r-0 dark:border-gray-600 rounded-l-lg flex items-center" data-svelte-h="svelte-9ujxug">#</div> <input class="w-full rounded-r-lg py-2.5 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 disabled:text-gray-500 dark:disabled:text-gray-500 outline-none svelte-1vx7r9s" type="text" autocomplete="off" required${add_attribute("value", doc.name, 0)}></div> </div> <div class="flex flex-col w-full"><div class="mb-1 text-xs text-gray-500" data-svelte-h="svelte-bs5oh2">Title</div> <div class="flex-1"><input class="w-full rounded-lg py-2.5 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none svelte-1vx7r9s" type="text" autocomplete="off" required${add_attribute("value", doc.title, 0)}></div></div> <div class="flex flex-col w-full"><div class="mb-1.5 text-xs text-gray-500" data-svelte-h="svelte-1tnbnwm">Tags</div> ${validate_component(Tags, "Tags").$$render(
            $$result,
            {
              tags,
              addTag: addTagHandler,
              deleteTag: deleteTagHandler
            },
            {},
            {}
          )}</div></div> <div class="flex justify-end pt-5 text-sm font-medium" data-svelte-h="svelte-1g42ope"><button class="px-4 py-2 bg-emerald-600 hover:bg-emerald-700 text-gray-100 transition rounded" type="submit">Save</button></div></form></div></div></div>`;
        }
      }
    )}`;
  } while (!$$settled);
  return $$rendered;
});
const General = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { saveHandler } = $$props;
  let chunkSize = 0;
  let chunkOverlap = 0;
  let querySettings = { template: "", k: 4 };
  if ($$props.saveHandler === void 0 && $$bindings.saveHandler && saveHandler !== void 0)
    $$bindings.saveHandler(saveHandler);
  return `<form class="flex flex-col h-full justify-between space-y-3 text-sm"><div class="space-y-3 pr-1.5 overflow-y-scroll max-h-80"><div><div class="mb-2 text-sm font-medium" data-svelte-h="svelte-cof781">General Settings</div> <div class="flex w-full justify-between"><div class="self-center text-xs font-medium" data-svelte-h="svelte-kbeq7u">Scan for documents from &#39;/data/docs&#39;</div> <button class="${"self-center text-xs p-1 px-3 bg-gray-100 dark:bg-gray-800 dark:hover:bg-gray-700 rounded flex flex-row space-x-1 items-center " + escape("", true)}" type="button" ${""}><div class="self-center font-medium" data-svelte-h="svelte-brpk47">Scan</div>  ${``}</button></div></div> <hr class="dark:border-gray-700"> <div class="space-y-3"><div class="space-y-3"><div class="text-sm font-medium" data-svelte-h="svelte-bs6nw7">Chunk Params</div> <div class="flex gap-2"><div class="flex w-full justify-between gap-2"><div class="self-center text-xs font-medium min-w-fit" data-svelte-h="svelte-zhubqx">Chunk Size</div> <div class="self-center"><input class="w-full rounded py-1.5 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none border border-gray-100 dark:border-gray-600" type="number" placeholder="Enter Chunk Size" autocomplete="off" min="0"${add_attribute("value", chunkSize, 0)}></div></div> <div class="flex w-full gap-2"><div class="self-center text-xs font-medium min-w-fit" data-svelte-h="svelte-dln3z5">Chunk Overlap</div> <div class="self-center"><input class="w-full rounded py-1.5 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none border border-gray-100 dark:border-gray-600" type="number" placeholder="Enter Chunk Overlap" autocomplete="off" min="0"${add_attribute("value", chunkOverlap, 0)}></div></div></div> <div><div class="flex justify-between items-center text-xs"><div class="text-xs font-medium" data-svelte-h="svelte-1t09rjz">PDF Extract Images (OCR)</div> <button class="text-xs font-medium text-gray-500" type="button">${escape("On")}</button></div></div></div> <div><div class="text-sm font-medium" data-svelte-h="svelte-1b76pt8">Query Params</div> <div class="flex py-2"><div class="flex w-full justify-between gap-2"><div class="self-center text-xs font-medium flex-1" data-svelte-h="svelte-1dvxwaa">Top K</div> <div class="self-center"><input class="w-full rounded py-1.5 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none border border-gray-100 dark:border-gray-600" type="number" placeholder="Enter Top K" autocomplete="off" min="0"${add_attribute("value", querySettings.k, 0)}></div></div> </div> <div><div class="mb-2.5 text-sm font-medium" data-svelte-h="svelte-ho3jbh">RAG Template</div> <textarea class="w-full rounded p-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none resize-none" rows="4">${escape("")}</textarea></div></div></div></div> <div class="flex justify-end pt-3 text-sm font-medium" data-svelte-h="svelte-1w4736w"><button class="px-4 py-2 bg-emerald-600 hover:bg-emerald-700 text-gray-100 transition rounded" type="submit">Save</button></div></form>`;
});
const SettingsModal = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { show = false } = $$props;
  if ($$props.show === void 0 && $$bindings.show && show !== void 0)
    $$bindings.show(show);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${validate_component(Modal, "Modal").$$render(
      $$result,
      { show },
      {
        show: ($$value) => {
          show = $$value;
          $$settled = false;
        }
      },
      {
        default: () => {
          return `<div><div class="flex justify-between dark:text-gray-300 px-5 py-4"><div class="text-lg font-medium self-center" data-svelte-h="svelte-a7e8yf">Document Settings</div> <button class="self-center" data-svelte-h="svelte-745w2y"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z"></path></svg></button></div> <hr class="dark:border-gray-800"> <div class="flex flex-col md:flex-row w-full p-4 md:space-x-4"><div class="tabs flex flex-row overflow-x-auto space-x-1 md:space-x-0 md:space-y-1 md:flex-col flex-1 md:flex-none md:w-40 dark:text-gray-200 text-xs text-left mb-3 md:mb-0"><button class="${"px-2.5 py-2.5 min-w-fit rounded-lg flex-1 md:flex-none flex text-right transition " + escape(
            "bg-gray-200 dark:bg-gray-700",
            true
          )}"><div class="self-center mr-2" data-svelte-h="svelte-1k1ieum"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M6.955 1.45A.5.5 0 0 1 7.452 1h1.096a.5.5 0 0 1 .497.45l.17 1.699c.484.12.94.312 1.356.562l1.321-1.081a.5.5 0 0 1 .67.033l.774.775a.5.5 0 0 1 .034.67l-1.08 1.32c.25.417.44.873.561 1.357l1.699.17a.5.5 0 0 1 .45.497v1.096a.5.5 0 0 1-.45.497l-1.699.17c-.12.484-.312.94-.562 1.356l1.082 1.322a.5.5 0 0 1-.034.67l-.774.774a.5.5 0 0 1-.67.033l-1.322-1.08c-.416.25-.872.44-1.356.561l-.17 1.699a.5.5 0 0 1-.497.45H7.452a.5.5 0 0 1-.497-.45l-.17-1.699a4.973 4.973 0 0 1-1.356-.562L4.108 13.37a.5.5 0 0 1-.67-.033l-.774-.775a.5.5 0 0 1-.034-.67l1.08-1.32a4.971 4.971 0 0 1-.561-1.357l-1.699-.17A.5.5 0 0 1 1 8.548V7.452a.5.5 0 0 1 .45-.497l1.699-.17c.12-.484.312-.94.562-1.356L2.629 4.107a.5.5 0 0 1 .034-.67l.774-.774a.5.5 0 0 1 .67-.033L5.43 3.71a4.97 4.97 0 0 1 1.356-.561l.17-1.699ZM6 8c0 .538.212 1.026.558 1.385l.057.057a2 2 0 0 0 2.828-2.828l-.058-.056A2 2 0 0 0 6 8Z" clip-rule="evenodd"></path></svg></div> <div class="self-center" data-svelte-h="svelte-115rgpl">General</div></button></div> <div class="flex-1 md:min-h-[380px]">${`${validate_component(General, "General").$$render(
            $$result,
            {
              saveHandler: () => {
                show = false;
              }
            },
            {},
            {}
          )}  `}</div></div></div>`;
        }
      }
    )}`;
  } while (!$$settled);
  return $$rendered;
});
const AddDocModal_svelte_svelte_type_style_lang = "";
const css = {
  code: "input.svelte-1vx7r9s::-webkit-outer-spin-button,input.svelte-1vx7r9s::-webkit-inner-spin-button{-webkit-appearance:none;margin:0}",
  map: null
};
const AddDocModal = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { show = false } = $$props;
  let { selectedDoc } = $$props;
  let tags = [];
  const addTagHandler = async (tagName) => {
    if (!tags.find((tag) => tag.name === tagName) && tagName !== "") {
      tags = [...tags, { name: tagName }];
    } else {
      console.log("tag already exists");
    }
  };
  const deleteTagHandler = async (tagName) => {
    tags = tags.filter((tag) => tag.name !== tagName);
  };
  if ($$props.show === void 0 && $$bindings.show && show !== void 0)
    $$bindings.show(show);
  if ($$props.selectedDoc === void 0 && $$bindings.selectedDoc && selectedDoc !== void 0)
    $$bindings.selectedDoc(selectedDoc);
  $$result.css.add(css);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${validate_component(Modal, "Modal").$$render(
      $$result,
      { size: "sm", show },
      {
        show: ($$value) => {
          show = $$value;
          $$settled = false;
        }
      },
      {
        default: () => {
          return `<div><div class="flex justify-between dark:text-gray-300 px-5 py-4"><div class="text-lg font-medium self-center" data-svelte-h="svelte-k9q435">Add Docs</div> <button class="self-center" data-svelte-h="svelte-745w2y"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z"></path></svg></button></div> <hr class="dark:border-gray-800"> <div class="flex flex-col md:flex-row w-full px-5 py-4 md:space-x-4 dark:text-gray-200"><div class="flex flex-col w-full sm:flex-row sm:justify-center sm:space-x-6"><form class="flex flex-col w-full"><div class="mb-3 w-full"><input id="upload-doc-input" hidden type="file" multiple class="svelte-1vx7r9s"> <button class="w-full text-sm font-medium py-3 bg-gray-850 hover:bg-gray-800 text-center rounded-xl" type="button">${`Click here to select documents.`}</button></div> <div class="flex flex-col space-y-1.5"><div class="flex flex-col w-full"><div class="mb-1.5 text-xs text-gray-500" data-svelte-h="svelte-1tnbnwm">Tags</div> ${validate_component(Tags, "Tags").$$render(
            $$result,
            {
              tags,
              addTag: addTagHandler,
              deleteTag: deleteTagHandler
            },
            {},
            {}
          )}</div></div> <div class="flex justify-end pt-5 text-sm font-medium" data-svelte-h="svelte-1g42ope"><button class="px-4 py-2 bg-emerald-600 hover:bg-emerald-700 text-gray-100 transition rounded" type="submit">Save</button></div></form></div></div></div>`;
        }
      }
    )}`;
  } while (!$$settled);
  return $$rendered;
});
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $documents, $$unsubscribe_documents;
  let $WEBUI_NAME, $$unsubscribe_WEBUI_NAME;
  $$unsubscribe_documents = subscribe(documents, (value) => $documents = value);
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => $WEBUI_NAME = value);
  let query = "";
  let tags = [];
  let showSettingsModal = false;
  let showAddDocModal = false;
  let showEditDocModal = false;
  let selectedDoc;
  let filteredDocs;
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    filteredDocs = $documents.filter((doc) => query === "");
    $$rendered = `${$$result.head += `<!-- HEAD_svelte-1jyvne9_START -->${$$result.title = `<title> ${escape(`Documents | ${$WEBUI_NAME}`)} </title>`, ""}<!-- HEAD_svelte-1jyvne9_END -->`, ""} ${``} ${validate_component(EditDocModal, "EditDocModal").$$render(
      $$result,
      { selectedDoc, show: showEditDocModal },
      {
        show: ($$value) => {
          showEditDocModal = $$value;
          $$settled = false;
        }
      },
      {}
    )} ${validate_component(AddDocModal, "AddDocModal").$$render(
      $$result,
      { show: showAddDocModal },
      {
        show: ($$value) => {
          showAddDocModal = $$value;
          $$settled = false;
        }
      },
      {}
    )} ${validate_component(SettingsModal, "SettingsModal").$$render(
      $$result,
      { show: showSettingsModal },
      {
        show: ($$value) => {
          showSettingsModal = $$value;
          $$settled = false;
        }
      },
      {}
    )} <div class="min-h-screen max-h-[100dvh] w-full flex justify-center dark:text-white"><div class="flex flex-col justify-between w-full overflow-y-auto"><div class="max-w-2xl mx-auto w-full px-3 md:px-0 my-10"><div class="mb-6"><div class="flex justify-between items-center"><div class="text-2xl font-semibold self-center" data-svelte-h="svelte-z8eqhm">My Documents</div> <div><button class="flex items-center space-x-1 px-3 py-1.5 rounded-xl bg-gray-50 hover:bg-gray-100 dark:bg-gray-800 dark:hover:bg-gray-700 transition" type="button" data-svelte-h="svelte-1kwaowx"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M6.955 1.45A.5.5 0 0 1 7.452 1h1.096a.5.5 0 0 1 .497.45l.17 1.699c.484.12.94.312 1.356.562l1.321-1.081a.5.5 0 0 1 .67.033l.774.775a.5.5 0 0 1 .034.67l-1.08 1.32c.25.417.44.873.561 1.357l1.699.17a.5.5 0 0 1 .45.497v1.096a.5.5 0 0 1-.45.497l-1.699.17c-.12.484-.312.94-.562 1.356l1.082 1.322a.5.5 0 0 1-.034.67l-.774.774a.5.5 0 0 1-.67.033l-1.322-1.08c-.416.25-.872.44-1.356.561l-.17 1.699a.5.5 0 0 1-.497.45H7.452a.5.5 0 0 1-.497-.45l-.17-1.699a4.973 4.973 0 0 1-1.356-.562L4.108 13.37a.5.5 0 0 1-.67-.033l-.774-.775a.5.5 0 0 1-.034-.67l1.08-1.32a4.971 4.971 0 0 1-.561-1.357l-1.699-.17A.5.5 0 0 1 1 8.548V7.452a.5.5 0 0 1 .45-.497l1.699-.17c.12-.484.312-.94.562-1.356L2.629 4.107a.5.5 0 0 1 .034-.67l.774-.774a.5.5 0 0 1 .67-.033L5.43 3.71a4.97 4.97 0 0 1 1.356-.561l.17-1.699ZM6 8c0 .538.212 1.026.558 1.385l.057.057a2 2 0 0 0 2.828-2.828l-.058-.056A2 2 0 0 0 6 8Z" clip-rule="evenodd"></path></svg> <div class="text-xs">Document Settings</div></button></div></div> <div class="text-gray-500 text-xs mt-1" data-svelte-h="svelte-41li7w">ⓘ Use &#39;#&#39; in the prompt input to load and select your documents.</div></div> <div class="flex w-full space-x-2"><div class="flex flex-1"><div class="self-center ml-1 mr-3" data-svelte-h="svelte-kinl2y"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z" clip-rule="evenodd"></path></svg></div> <input class="w-full text-sm pr-4 py-1 rounded-r-xl outline-none bg-transparent" placeholder="Search Document"${add_attribute("value", query, 0)}></div> <div><button class="px-2 py-2 rounded-xl border border-gray-200 dark:border-gray-600 dark:border-0 hover:bg-gray-100 dark:bg-gray-800 dark:hover:bg-gray-700 transition font-medium text-sm flex items-center space-x-1" data-svelte-h="svelte-1mfes9a"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path d="M8.75 3.75a.75.75 0 0 0-1.5 0v3.5h-3.5a.75.75 0 0 0 0 1.5h3.5v3.5a.75.75 0 0 0 1.5 0v-3.5h3.5a.75.75 0 0 0 0-1.5h-3.5v-3.5Z"></path></svg></button></div></div>  <hr class="dark:border-gray-700 my-2.5"> ${tags.length > 0 ? `<div class="px-2.5 pt-1 flex gap-1 flex-wrap"><div class="ml-0.5 pr-3 my-auto flex items-center">${validate_component(Checkbox, "Checkbox").$$render(
      $$result,
      {
        state: filteredDocs.filter((doc) => doc?.selected === "checked").length === filteredDocs.length ? "checked" : "unchecked",
        indeterminate: filteredDocs.filter((doc) => doc?.selected === "checked").length > 0 && filteredDocs.filter((doc) => doc?.selected === "checked").length !== filteredDocs.length
      },
      {},
      {}
    )}</div> ${filteredDocs.filter((doc) => doc?.selected === "checked").length === 0 ? `<button class="px-2 py-0.5 space-x-1 flex h-fit items-center rounded-full transition bg-gray-50 hover:bg-gray-100 dark:bg-gray-800 dark:text-white" data-svelte-h="svelte-1tfinx6"><div class="text-xs font-medium self-center line-clamp-1">all</div></button> ${each(tags, (tag) => {
      return `<button class="px-2 py-0.5 space-x-1 flex h-fit items-center rounded-full transition bg-gray-50 hover:bg-gray-100 dark:bg-gray-800 dark:text-white"><div class="text-xs font-medium self-center line-clamp-1">#${escape(tag)}</div> </button>`;
    })}` : `<div class="flex-1 flex w-full justify-between items-center"><div class="text-xs font-medium py-0.5 self-center mr-1">${escape(filteredDocs.filter((doc) => doc?.selected === "checked").length)} Selected</div> <div class="flex gap-1"> <button class="px-2 py-0.5 space-x-1 flex h-fit items-center rounded-full transition bg-gray-50 hover:bg-gray-100 dark:bg-gray-800 dark:text-white" data-svelte-h="svelte-xmow8f"><div class="text-xs font-medium self-center line-clamp-1">delete</div></button></div></div>`}</div>` : ``} <div class="my-3 mb-5">${each(filteredDocs, (doc) => {
      return `<button class="flex space-x-4 cursor-pointer text-left w-full px-3 py-2 dark:hover:bg-white/5 hover:bg-black/5 rounded-xl"><div class="my-auto flex items-center">${validate_component(Checkbox, "Checkbox").$$render($$result, { state: doc?.selected ?? "unchecked" }, {}, {})}</div> <div class="flex flex-1 space-x-4 cursor-pointer w-full"><div class="flex items-center space-x-3"><div class="p-2.5 bg-red-400 text-white rounded-lg">${doc ? `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6"><path fill-rule="evenodd" d="M5.625 1.5c-1.036 0-1.875.84-1.875 1.875v17.25c0 1.035.84 1.875 1.875 1.875h12.75c1.035 0 1.875-.84 1.875-1.875V12.75A3.75 3.75 0 0 0 16.5 9h-1.875a1.875 1.875 0 0 1-1.875-1.875V5.25A3.75 3.75 0 0 0 9 1.5H5.625ZM7.5 15a.75.75 0 0 1 .75-.75h7.5a.75.75 0 0 1 0 1.5h-7.5A.75.75 0 0 1 7.5 15Zm.75 2.25a.75.75 0 0 0 0 1.5H12a.75.75 0 0 0 0-1.5H8.25Z" clip-rule="evenodd"></path><path d="M12.971 1.816A5.23 5.23 0 0 1 14.25 5.25v1.875c0 .207.168.375.375.375H16.5a5.23 5.23 0 0 1 3.434 1.279 9.768 9.768 0 0 0-6.963-6.963Z"></path></svg>` : `<svg class="w-6 h-6 translate-y-[0.5px]" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><style>.spinner_qM83 {
													animation: spinner_8HQG 1.05s infinite;
												}
												.spinner_oXPr {
													animation-delay: 0.1s;
												}
												.spinner_ZTLf {
													animation-delay: 0.2s;
												}
												@keyframes spinner_8HQG {
													0%,
													57.14% {
														animation-timing-function: cubic-bezier(0.33, 0.66, 0.66, 1);
														transform: translate(0);
													}
													28.57% {
														animation-timing-function: cubic-bezier(0.33, 0, 0.66, 0.33);
														transform: translateY(-6px);
													}
													100% {
														transform: translate(0);
													}
												}
											</style><circle class="spinner_qM83" cx="4" cy="12" r="2.5"></circle><circle class="spinner_qM83 spinner_oXPr" cx="12" cy="12" r="2.5"></circle><circle class="spinner_qM83 spinner_ZTLf" cx="20" cy="12" r="2.5"></circle></svg>`}</div> <div class="self-center flex-1"><div class="font-bold line-clamp-1">#${escape(doc.name)} (${escape(doc.filename)})</div> <div class="text-xs overflow-hidden text-ellipsis line-clamp-1">${escape(doc.title)} </div></div> </div></div> <div class="flex flex-row space-x-1 self-center"><button class="self-center w-fit text-sm z-20 px-2 py-2 dark:text-gray-300 dark:hover:text-white hover:bg-black/5 dark:hover:bg-white/5 rounded-xl" type="button" data-svelte-h="svelte-mdhrvt"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125"></path></svg></button>  <button class="self-center w-fit text-sm px-2 py-2 dark:text-gray-300 dark:hover:text-white hover:bg-black/5 dark:hover:bg-white/5 rounded-xl" type="button" data-svelte-h="svelte-smh81r"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"></path></svg> </button></div> </button>`;
    })}</div> <div class="flex justify-end w-full mb-2"><div class="flex space-x-2"><input id="documents-import-input" type="file" accept=".json" hidden> <button class="flex text-xs items-center space-x-1 px-3 py-1.5 rounded-xl bg-gray-50 hover:bg-gray-100 dark:bg-gray-800 dark:hover:bg-gray-700 dark:text-gray-200 transition" data-svelte-h="svelte-g8msjz"><div class="self-center mr-2 font-medium">Import Documents Mapping</div> <div class="self-center"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M4 2a1.5 1.5 0 0 0-1.5 1.5v9A1.5 1.5 0 0 0 4 14h8a1.5 1.5 0 0 0 1.5-1.5V6.621a1.5 1.5 0 0 0-.44-1.06L9.94 2.439A1.5 1.5 0 0 0 8.878 2H4Zm4 9.5a.75.75 0 0 1-.75-.75V8.06l-.72.72a.75.75 0 0 1-1.06-1.06l2-2a.75.75 0 0 1 1.06 0l2 2a.75.75 0 1 1-1.06 1.06l-.72-.72v2.69a.75.75 0 0 1-.75.75Z" clip-rule="evenodd"></path></svg></div></button> <button class="flex text-xs items-center space-x-1 px-3 py-1.5 rounded-xl bg-gray-50 hover:bg-gray-100 dark:bg-gray-800 dark:hover:bg-gray-700 dark:text-gray-200 transition" data-svelte-h="svelte-1gdyd1"><div class="self-center mr-2 font-medium">Export Documents Mapping</div> <div class="self-center"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M4 2a1.5 1.5 0 0 0-1.5 1.5v9A1.5 1.5 0 0 0 4 14h8a1.5 1.5 0 0 0 1.5-1.5V6.621a1.5 1.5 0 0 0-.44-1.06L9.94 2.439A1.5 1.5 0 0 0 8.878 2H4Zm4 3.5a.75.75 0 0 1 .75.75v2.69l.72-.72a.75.75 0 1 1 1.06 1.06l-2 2a.75.75 0 0 1-1.06 0l-2-2a.75.75 0 0 1 1.06-1.06l.72.72V6.25A.75.75 0 0 1 8 5.5Z" clip-rule="evenodd"></path></svg></div></button></div></div></div></div></div>`;
  } while (!$$settled);
  $$unsubscribe_documents();
  $$unsubscribe_WEBUI_NAME();
  return $$rendered;
});
export {
  Page as default
};
