import { c as create_ssr_component, p as createEventDispatcher, v as validate_component, b as add_attribute, f as escape, a as subscribe } from "../../../../chunks/ssr.js";
import { u as user, W as WEBUI_NAME } from "../../../../chunks/index3.js";
import "../../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import dayjs from "dayjs";
import { M as Modal } from "../../../../chunks/Modal.js";
const EditUserModal_svelte_svelte_type_style_lang = "";
const css$1 = {
  code: "input.svelte-1vx7r9s::-webkit-outer-spin-button,input.svelte-1vx7r9s::-webkit-inner-spin-button{-webkit-appearance:none;margin:0}",
  map: null
};
const EditUserModal = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  createEventDispatcher();
  let { show = false } = $$props;
  let { selectedUser } = $$props;
  let { sessionUser } = $$props;
  let _user = {
    profile_image_url: "",
    name: "",
    email: "",
    password: ""
  };
  if ($$props.show === void 0 && $$bindings.show && show !== void 0)
    $$bindings.show(show);
  if ($$props.selectedUser === void 0 && $$bindings.selectedUser && selectedUser !== void 0)
    $$bindings.selectedUser(selectedUser);
  if ($$props.sessionUser === void 0 && $$bindings.sessionUser && sessionUser !== void 0)
    $$bindings.sessionUser(sessionUser);
  $$result.css.add(css$1);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${validate_component(Modal, "Modal").$$render(
      $$result,
      { size: "sm", show },
      {
        show: ($$value) => {
          show = $$value;
          $$settled = false;
        }
      },
      {
        default: () => {
          return `<div><div class="flex justify-between dark:text-gray-300 px-5 py-4"><div class="text-lg font-medium self-center" data-svelte-h="svelte-160glhw">Edit User</div> <button class="self-center" data-svelte-h="svelte-745w2y"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z"></path></svg></button></div> <hr class="dark:border-gray-800"> <div class="flex flex-col md:flex-row w-full p-5 md:space-x-4 dark:text-gray-200"><div class="flex flex-col w-full sm:flex-row sm:justify-center sm:space-x-6"><form class="flex flex-col w-full"><div class="flex items-center rounded-md py-2 px-4 w-full"><div class="self-center mr-5"><img${add_attribute("src", selectedUser.profile_image_url, 0)} class="max-w-[55px] object-cover rounded-full" alt="User profile"></div> <div><div class="self-center capitalize font-semibold">${escape(selectedUser.name)}</div> <div class="text-xs text-gray-500">Created at ${escape(dayjs(selectedUser.timestamp * 1e3).format("MMMM DD, YYYY"))}</div></div></div> <hr class="dark:border-gray-800 my-3 w-full"> <div class="flex flex-col space-y-1.5"><div class="flex flex-col w-full"><div class="mb-1 text-xs text-gray-500" data-svelte-h="svelte-5xj5cw">Email</div> <div class="flex-1"><input class="w-full rounded py-2 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 disabled:text-gray-500 dark:disabled:text-gray-500 outline-none svelte-1vx7r9s" type="email" autocomplete="off" required ${_user.id == sessionUser.id ? "disabled" : ""}${add_attribute("value", _user.email, 0)}></div></div> <div class="flex flex-col w-full"><div class="mb-1 text-xs text-gray-500" data-svelte-h="svelte-guynw3">Name</div> <div class="flex-1"><input class="w-full rounded py-2 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none svelte-1vx7r9s" type="text" autocomplete="off" required${add_attribute("value", _user.name, 0)}></div></div> <div class="flex flex-col w-full"><div class="mb-1 text-xs text-gray-500" data-svelte-h="svelte-1jv7gzz">New Password</div> <div class="flex-1"><input class="w-full rounded py-2 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none svelte-1vx7r9s" type="password" autocomplete="new-password"${add_attribute("value", _user.password, 0)}></div></div></div> <div class="flex justify-end pt-3 text-sm font-medium" data-svelte-h="svelte-itb9qk"><button class="px-4 py-2 bg-emerald-600 hover:bg-emerald-700 text-gray-100 transition rounded" type="submit">Save</button></div></form></div></div></div>`;
        }
      }
    )}`;
  } while (!$$settled);
  return $$rendered;
});
const General = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { saveHandler } = $$props;
  let JWTExpiresIn = "";
  if ($$props.saveHandler === void 0 && $$bindings.saveHandler && saveHandler !== void 0)
    $$bindings.saveHandler(saveHandler);
  return `<form class="flex flex-col h-full justify-between space-y-3 text-sm"><div class="space-y-3 pr-1.5 overflow-y-scroll max-h-80"><div><div class="mb-2 text-sm font-medium" data-svelte-h="svelte-cof781">General Settings</div> <div class="flex w-full justify-between"><div class="self-center text-xs font-medium" data-svelte-h="svelte-sto0mt">Enable New Sign Ups</div> <button class="p-1 px-3 text-xs flex rounded transition" type="button">${`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path d="M11.5 1A3.5 3.5 0 0 0 8 4.5V7H2.5A1.5 1.5 0 0 0 1 8.5v5A1.5 1.5 0 0 0 2.5 15h7a1.5 1.5 0 0 0 1.5-1.5v-5A1.5 1.5 0 0 0 9.5 7V4.5a2 2 0 1 1 4 0v1.75a.75.75 0 0 0 1.5 0V4.5A3.5 3.5 0 0 0 11.5 1Z"></path></svg> <span class="ml-2 self-center" data-svelte-h="svelte-q4m40m">Enabled</span>`}</button></div> <div class="flex w-full justify-between"><div class="self-center text-xs font-medium" data-svelte-h="svelte-jicu4p">Default User Role</div> <div class="flex items-center relative"><select class="w-fit pr-8 rounded py-2 px-2 text-xs bg-transparent outline-none text-right" placeholder="Select a theme"><option value="pending" data-svelte-h="svelte-mygx0s">Pending</option><option value="user" data-svelte-h="svelte-1u8kdru">User</option><option value="admin" data-svelte-h="svelte-17op260">Admin</option></select></div></div> <hr class="dark:border-gray-700 my-3"> <div class="w-full justify-between"><div class="flex w-full justify-between" data-svelte-h="svelte-3t3pvh"><div class="self-center text-xs font-medium">JWT Expiration</div></div> <div class="flex mt-2 space-x-2"><input class="w-full rounded py-1.5 px-4 text-sm dark:text-gray-300 dark:bg-gray-800 outline-none border border-gray-100 dark:border-gray-600" type="text"${add_attribute("placeholder", `e.g.) "30m","1h", "10d". `, 0)}${add_attribute("value", JWTExpiresIn, 0)}></div> <div class="mt-2 text-xs text-gray-400 dark:text-gray-500" data-svelte-h="svelte-1g534va">Valid time units: <span class="text-gray-300 font-medium">&#39;s&#39;, &#39;m&#39;, &#39;h&#39;, &#39;d&#39;, &#39;w&#39; or &#39;-1&#39; for no expiration.</span></div></div></div></div> <div class="flex justify-end pt-3 text-sm font-medium" data-svelte-h="svelte-1w4736w"><button class="px-4 py-2 bg-emerald-600 hover:bg-emerald-700 text-gray-100 transition rounded" type="submit">Save</button></div></form>`;
});
const SettingsModal = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { show = false } = $$props;
  if ($$props.show === void 0 && $$bindings.show && show !== void 0)
    $$bindings.show(show);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${validate_component(Modal, "Modal").$$render(
      $$result,
      { show },
      {
        show: ($$value) => {
          show = $$value;
          $$settled = false;
        }
      },
      {
        default: () => {
          return `<div><div class="flex justify-between dark:text-gray-300 px-5 py-4"><div class="text-lg font-medium self-center" data-svelte-h="svelte-3t1yg3">Admin Settings</div> <button class="self-center" data-svelte-h="svelte-745w2y"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z"></path></svg></button></div> <hr class="dark:border-gray-800"> <div class="flex flex-col md:flex-row w-full p-4 md:space-x-4"><div class="tabs flex flex-row overflow-x-auto space-x-1 md:space-x-0 md:space-y-1 md:flex-col flex-1 md:flex-none md:w-40 dark:text-gray-200 text-xs text-left mb-3 md:mb-0"><button class="${"px-2.5 py-2.5 min-w-fit rounded-lg flex-1 md:flex-none flex text-right transition " + escape(
            "bg-gray-200 dark:bg-gray-700",
            true
          )}"><div class="self-center mr-2" data-svelte-h="svelte-1k1ieum"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M6.955 1.45A.5.5 0 0 1 7.452 1h1.096a.5.5 0 0 1 .497.45l.17 1.699c.484.12.94.312 1.356.562l1.321-1.081a.5.5 0 0 1 .67.033l.774.775a.5.5 0 0 1 .034.67l-1.08 1.32c.25.417.44.873.561 1.357l1.699.17a.5.5 0 0 1 .45.497v1.096a.5.5 0 0 1-.45.497l-1.699.17c-.12.484-.312.94-.562 1.356l1.082 1.322a.5.5 0 0 1-.034.67l-.774.774a.5.5 0 0 1-.67.033l-1.322-1.08c-.416.25-.872.44-1.356.561l-.17 1.699a.5.5 0 0 1-.497.45H7.452a.5.5 0 0 1-.497-.45l-.17-1.699a4.973 4.973 0 0 1-1.356-.562L4.108 13.37a.5.5 0 0 1-.67-.033l-.774-.775a.5.5 0 0 1-.034-.67l1.08-1.32a4.971 4.971 0 0 1-.561-1.357l-1.699-.17A.5.5 0 0 1 1 8.548V7.452a.5.5 0 0 1 .45-.497l1.699-.17c.12-.484.312-.94.562-1.356L2.629 4.107a.5.5 0 0 1 .034-.67l.774-.774a.5.5 0 0 1 .67-.033L5.43 3.71a4.97 4.97 0 0 1 1.356-.561l.17-1.699ZM6 8c0 .538.212 1.026.558 1.385l.057.057a2 2 0 0 0 2.828-2.828l-.058-.056A2 2 0 0 0 6 8Z" clip-rule="evenodd"></path></svg></div> <div class="self-center" data-svelte-h="svelte-115rgpl">General</div></button> <button class="${"px-2.5 py-2.5 min-w-fit rounded-lg flex-1 md:flex-none flex text-right transition " + escape(
            " hover:bg-gray-300 dark:hover:bg-gray-800",
            true
          )}"><div class="self-center mr-2" data-svelte-h="svelte-1cwxqsc"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path d="M8 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5ZM3.156 11.763c.16-.629.44-1.21.813-1.72a2.5 2.5 0 0 0-2.725 1.377c-.136.287.102.58.418.58h1.449c.01-.077.025-.156.045-.237ZM12.847 11.763c.02.08.036.16.046.237h1.446c.316 0 .554-.293.417-.579a2.5 2.5 0 0 0-2.722-1.378c.374.51.653 1.09.813 1.72ZM14 7.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0ZM3.5 9a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3ZM5 13c-.552 0-1.013-.455-.876-.99a4.002 4.002 0 0 1 7.753 0c.136.535-.324.99-.877.99H5Z"></path></svg></div> <div class="self-center" data-svelte-h="svelte-1sxrt8x">Users</div></button> <button class="${"px-2.5 py-2.5 min-w-fit rounded-lg flex-1 md:flex-none flex text-right transition " + escape(
            " hover:bg-gray-300 dark:hover:bg-gray-800",
            true
          )}"><div class="self-center mr-2" data-svelte-h="svelte-1xqf9ou"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path d="M8 7c3.314 0 6-1.343 6-3s-2.686-3-6-3-6 1.343-6 3 2.686 3 6 3Z"></path><path d="M8 8.5c1.84 0 3.579-.37 4.914-1.037A6.33 6.33 0 0 0 14 6.78V8c0 1.657-2.686 3-6 3S2 9.657 2 8V6.78c.346.273.72.5 1.087.683C4.42 8.131 6.16 8.5 8 8.5Z"></path><path d="M8 12.5c1.84 0 3.579-.37 4.914-1.037.366-.183.74-.41 1.086-.684V12c0 1.657-2.686 3-6 3s-6-1.343-6-3v-1.22c.346.273.72.5 1.087.683C4.42 12.131 6.16 12.5 8 12.5Z"></path></svg></div> <div class="self-center" data-svelte-h="svelte-102m5xa">Database</div></button></div> <div class="flex-1 md:min-h-[380px]">${`${validate_component(General, "General").$$render(
            $$result,
            {
              saveHandler: () => {
                show = false;
              }
            },
            {},
            {}
          )}`}</div></div></div>`;
        }
      }
    )}`;
  } while (!$$settled);
  return $$rendered;
});
const _page_svelte_svelte_type_style_lang = "";
const css = {
  code: ".font-mona.svelte-3g4avz{font-family:'Mona Sans'}.scrollbar-hidden.svelte-3g4avz::-webkit-scrollbar{display:none}.scrollbar-hidden.svelte-3g4avz{-ms-overflow-style:none;scrollbar-width:none}",
  map: null
};
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $user, $$unsubscribe_user;
  let $WEBUI_NAME, $$unsubscribe_WEBUI_NAME;
  $$unsubscribe_user = subscribe(user, (value) => $user = value);
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => $WEBUI_NAME = value);
  let selectedUser = null;
  let showSettingsModal = false;
  let showEditUserModal = false;
  $$result.css.add(css);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${$$result.head += `<!-- HEAD_svelte-19gxtrm_START -->${$$result.title = `<title> ${escape(`Admin Panel | ${$WEBUI_NAME}`)} </title>`, ""}<!-- HEAD_svelte-19gxtrm_END -->`, ""} ${validate_component(EditUserModal, "EditUserModal").$$render(
      $$result,
      {
        selectedUser,
        sessionUser: $user,
        show: showEditUserModal
      },
      {
        show: ($$value) => {
          showEditUserModal = $$value;
          $$settled = false;
        }
      },
      {}
    )} ${validate_component(SettingsModal, "SettingsModal").$$render(
      $$result,
      { show: showSettingsModal },
      {
        show: ($$value) => {
          showSettingsModal = $$value;
          $$settled = false;
        }
      },
      {}
    )} <div class="min-h-screen max-h-[100dvh] w-full flex justify-center dark:text-white font-mona svelte-3g4avz">${``} </div>`;
  } while (!$$settled);
  $$unsubscribe_user();
  $$unsubscribe_WEBUI_NAME();
  return $$rendered;
});
export {
  Page as default
};
