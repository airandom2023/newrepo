import { c as create_ssr_component, a as subscribe } from "../../../chunks/ssr.js";
import "../../../chunks/Toaster.svelte_svelte_type_style_lang.js";
import "idb";
import "file-saver";
import { c as config, u as user, s as showSettings } from "../../../chunks/index3.js";
import "js-sha256";
/* empty css                                                   */import "async/queue.js";
/* empty css                                                            */const SettingsModal_svelte_svelte_type_style_lang = "";
const _layout_svelte_svelte_type_style_lang = "";
const css = {
  code: "@keyframes svelte-19023qi-l{to{clip-path:inset(0 -1ch 0 0)}}",
  map: null
};
const Layout = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_config;
  let $$unsubscribe_user;
  let $$unsubscribe_showSettings;
  $$unsubscribe_config = subscribe(config, (value) => value);
  $$unsubscribe_user = subscribe(user, (value) => value);
  $$unsubscribe_showSettings = subscribe(showSettings, (value) => value);
  $$result.css.add(css);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${``}`;
  } while (!$$settled);
  $$unsubscribe_config();
  $$unsubscribe_user();
  $$unsubscribe_showSettings();
  return $$rendered;
});
export {
  Layout as default
};
