import { c as create_ssr_component, a as subscribe } from "../../../chunks/ssr.js";
import { c as config, W as WEBUI_NAME } from "../../../chunks/index3.js";
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_config;
  let $$unsubscribe_WEBUI_NAME;
  $$unsubscribe_config = subscribe(config, (value) => value);
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => value);
  $$unsubscribe_config();
  $$unsubscribe_WEBUI_NAME();
  return `${``}`;
});
export {
  Page as default
};
