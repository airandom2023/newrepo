

export const index = 1;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/_error.svelte.js')).default;
export const imports = ["_app/immutable/nodes/1.5f5ff7b9.js","_app/immutable/chunks/scheduler.83ca7f71.js","_app/immutable/chunks/index.2dc1c432.js","_app/immutable/chunks/stores.28dec1aa.js","_app/immutable/chunks/singletons.a4e6986a.js","_app/immutable/chunks/index.40add014.js"];
export const stylesheets = [];
export const fonts = [];
