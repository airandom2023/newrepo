import * as universal from '../entries/pages/_layout.js';

export const index = 0;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/_layout.svelte.js')).default;
export { universal };
export const universal_id = "src/routes/+layout.js";
export const imports = ["_app/immutable/nodes/0.4d79243e.js","_app/immutable/chunks/scheduler.83ca7f71.js","_app/immutable/chunks/index.2dc1c432.js","_app/immutable/chunks/index.1b124964.js","_app/immutable/chunks/index.40add014.js","_app/immutable/chunks/navigation.af5274a5.js","_app/immutable/chunks/singletons.a4e6986a.js","_app/immutable/chunks/globals.7f7f1b26.js","_app/immutable/chunks/each.55c2b601.js","_app/immutable/chunks/spread.8a54911c.js","_app/immutable/chunks/Toaster.svelte_svelte_type_style_lang.fb21b1a3.js","_app/immutable/chunks/index.86c78391.js","_app/immutable/chunks/index.f830b11d.js"];
export const stylesheets = ["_app/immutable/assets/0.232144e1.css","_app/immutable/assets/Toaster.ebb080d6.css"];
export const fonts = [];
