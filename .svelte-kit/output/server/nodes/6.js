

export const index = 6;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/(app)/documents/_page.svelte.js')).default;
export const imports = ["_app/immutable/nodes/6.78f7c4aa.js","_app/immutable/chunks/scheduler.83ca7f71.js","_app/immutable/chunks/index.2dc1c432.js","_app/immutable/chunks/globals.7f7f1b26.js","_app/immutable/chunks/each.55c2b601.js","_app/immutable/chunks/Toaster.svelte_svelte_type_style_lang.fb21b1a3.js","_app/immutable/chunks/index.40add014.js","_app/immutable/chunks/FileSaver.min.898eb36f.js","_app/immutable/chunks/_commonjsHelpers.de833af9.js","_app/immutable/chunks/index.1b124964.js","_app/immutable/chunks/index.fcb7c064.js","_app/immutable/chunks/index.671e6d60.js","_app/immutable/chunks/index.b3db03b7.js","_app/immutable/chunks/dayjs.min.1e504c00.js","_app/immutable/chunks/Modal.6be87821.js","_app/immutable/chunks/Tags.228730a4.js"];
export const stylesheets = ["_app/immutable/assets/6.c242e6f1.css","_app/immutable/assets/Toaster.ebb080d6.css","_app/immutable/assets/Modal.beb6c5d8.css"];
export const fonts = [];
