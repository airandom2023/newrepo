

export const index = 15;
let component_cache;
export const component = async () => component_cache ??= (await import('../entries/pages/error/_page.svelte.js')).default;
export const imports = ["_app/immutable/nodes/15.c47a07da.js","_app/immutable/chunks/scheduler.83ca7f71.js","_app/immutable/chunks/index.2dc1c432.js","_app/immutable/chunks/navigation.af5274a5.js","_app/immutable/chunks/singletons.a4e6986a.js","_app/immutable/chunks/index.40add014.js","_app/immutable/chunks/index.1b124964.js"];
export const stylesheets = [];
export const fonts = [];
