import { w as writable } from "./index2.js";
const APP_NAME = "VayuGPT";
const WEBUI_BASE_URL = ``;
const WEBUI_API_BASE_URL = `${WEBUI_BASE_URL}/api/v1`;
const OLLAMA_API_BASE_URL = `${WEBUI_BASE_URL}/ollama`;
const RAG_API_BASE_URL = `${WEBUI_BASE_URL}/rag/api/v1`;
const WEBUI_NAME = writable(APP_NAME);
const config = writable(void 0);
const user = writable(void 0);
const chatId = writable("");
const chats = writable([]);
const tags = writable([]);
const models = writable([]);
const releventContexts = writable([]);
const modelfiles = writable([]);
const prompts = writable([]);
const documents = writable([
  {
    collection_name: "collection_name",
    filename: "filename",
    name: "name",
    title: "title"
  },
  {
    collection_name: "collection_name1",
    filename: "filename1",
    name: "name1",
    title: "title1"
  }
]);
const settings = writable({});
const showSettings = writable(false);
export {
  OLLAMA_API_BASE_URL as O,
  RAG_API_BASE_URL as R,
  WEBUI_NAME as W,
  WEBUI_BASE_URL as a,
  settings as b,
  config as c,
  chatId as d,
  documents as e,
  modelfiles as f,
  chats as g,
  WEBUI_API_BASE_URL as h,
  models as m,
  prompts as p,
  releventContexts as r,
  showSettings as s,
  tags as t,
  user as u
};
