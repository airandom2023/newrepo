import { c as create_ssr_component, p as createEventDispatcher, f as escape, g as each, v as validate_component } from "./ssr.js";
const TagInput = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  createEventDispatcher();
  return `<div class="flex space-x-1 pl-1.5">${``} <button class="cursor-pointer self-center p-0.5 space-x-1 flex h-fit items-center dark:hover:bg-gray-700 rounded-full transition border dark:border-gray-600 border-dashed" type="button"><div class="m-auto self-center"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="${"w-3 h-3 " + escape("", true) + " transition-all transform"}"><path d="M8.75 3.75a.75.75 0 0 0-1.5 0v3.5h-3.5a.75.75 0 0 0 0 1.5h3.5v3.5a.75.75 0 0 0 1.5 0v-3.5h3.5a.75.75 0 0 0 0-1.5h-3.5v-3.5Z"></path></svg></div></button></div>`;
});
const TagList = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  createEventDispatcher();
  let { tags = [] } = $$props;
  if ($$props.tags === void 0 && $$bindings.tags && tags !== void 0)
    $$bindings.tags(tags);
  return `${each(tags, (tag) => {
    return `<div class="px-2 py-0.5 space-x-1 flex h-fit items-center rounded-full transition border dark:border-gray-600 dark:text-white"><div class="text-[0.7rem] font-medium self-center line-clamp-1">${escape(tag.name)}</div> <button class="m-auto self-center cursor-pointer" data-svelte-h="svelte-1uiqlcf"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-3 h-3"><path d="M5.28 4.22a.75.75 0 0 0-1.06 1.06L6.94 8l-2.72 2.72a.75.75 0 1 0 1.06 1.06L8 9.06l2.72 2.72a.75.75 0 1 0 1.06-1.06L9.06 8l2.72-2.72a.75.75 0 0 0-1.06-1.06L8 6.94 5.28 4.22Z"></path></svg></button> </div>`;
  })}`;
});
const Tags = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { tags = [] } = $$props;
  let { deleteTag } = $$props;
  let { addTag } = $$props;
  if ($$props.tags === void 0 && $$bindings.tags && tags !== void 0)
    $$bindings.tags(tags);
  if ($$props.deleteTag === void 0 && $$bindings.deleteTag && deleteTag !== void 0)
    $$bindings.deleteTag(deleteTag);
  if ($$props.addTag === void 0 && $$bindings.addTag && addTag !== void 0)
    $$bindings.addTag(addTag);
  return `<div class="flex flex-row space-x-0.5 line-clamp-1">${validate_component(TagList, "TagList").$$render($$result, { tags }, {}, {})} ${validate_component(TagInput, "TagInput").$$render($$result, {}, {}, {})}</div>`;
});
export {
  Tags as T
};
