import { c as create_ssr_component, f as escape } from "./ssr.js";
/* empty css                                     */const css = {
  code: ".modal-content.svelte-fq1rhy{animation:svelte-fq1rhy-scaleUp 0.1s ease-out forwards}@keyframes svelte-fq1rhy-scaleUp{from{transform:scale(0.985);opacity:0}to{transform:scale(1);opacity:1}}",
  map: null
};
const Modal = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { show = true } = $$props;
  let { size = "md" } = $$props;
  const sizeToWidth = (size2) => {
    if (size2 === "xs") {
      return "w-[16rem]";
    } else if (size2 === "sm") {
      return "w-[30rem]";
    } else {
      return "w-[44rem]";
    }
  };
  if ($$props.show === void 0 && $$bindings.show && show !== void 0)
    $$bindings.show(show);
  if ($$props.size === void 0 && $$bindings.size && size !== void 0)
    $$bindings.size(size);
  $$result.css.add(css);
  return `${show ? `  <div class="fixed top-0 right-0 left-0 bottom-0 bg-black/60 w-full min-h-screen h-screen flex justify-center z-50 overflow-hidden overscroll-contain"><div class="${"modal-content m-auto rounded-2xl max-w-full " + escape(sizeToWidth(size), true) + " mx-2 bg-gray-50 dark:bg-gray-900 shadow-3xl svelte-fq1rhy"}">${slots.default ? slots.default({}) : ``}</div></div>` : ``}`;
});
export {
  Modal as M
};
