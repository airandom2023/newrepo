import { O as OLLAMA_API_BASE_URL, h as WEBUI_API_BASE_URL, p as prompts, e as documents, m as models, b as settings, f as modelfiles, a as WEBUI_BASE_URL, c as config, u as user, g as chats, W as WEBUI_NAME } from "./index3.js";
import { v4 } from "uuid";
import "js-sha256";
import { q as set_current_component, r as run_all, t as current_component, c as create_ssr_component, o as onDestroy, b as add_attribute, a as subscribe, g as each, f as escape, p as createEventDispatcher, v as validate_component } from "./ssr.js";
import { a as toast } from "./Toaster.svelte_svelte_type_style_lang.js";
import dayjs from "dayjs";
import { marked } from "marked";
import tippy from "tippy.js";
import auto_render from "katex/dist/contrib/auto-render.mjs";
import hljs from "highlight.js";
import "file-saver";
/* empty css                                     */import { T as Tags } from "./Tags.js";
const dirty_components = [];
const binding_callbacks = [];
let render_callbacks = [];
const flush_callbacks = [];
const resolved_promise = /* @__PURE__ */ Promise.resolve();
let update_scheduled = false;
function schedule_update() {
  if (!update_scheduled) {
    update_scheduled = true;
    resolved_promise.then(flush);
  }
}
function tick() {
  schedule_update();
  return resolved_promise;
}
function add_render_callback(fn) {
  render_callbacks.push(fn);
}
const seen_callbacks = /* @__PURE__ */ new Set();
let flushidx = 0;
function flush() {
  if (flushidx !== 0) {
    return;
  }
  const saved_component = current_component;
  do {
    try {
      while (flushidx < dirty_components.length) {
        const component = dirty_components[flushidx];
        flushidx++;
        set_current_component(component);
        update(component.$$);
      }
    } catch (e) {
      dirty_components.length = 0;
      flushidx = 0;
      throw e;
    }
    set_current_component(null);
    dirty_components.length = 0;
    flushidx = 0;
    while (binding_callbacks.length)
      binding_callbacks.pop()();
    for (let i = 0; i < render_callbacks.length; i += 1) {
      const callback = render_callbacks[i];
      if (!seen_callbacks.has(callback)) {
        seen_callbacks.add(callback);
        callback();
      }
    }
    render_callbacks.length = 0;
  } while (dirty_components.length);
  while (flush_callbacks.length) {
    flush_callbacks.pop()();
  }
  update_scheduled = false;
  seen_callbacks.clear();
  set_current_component(saved_component);
}
function update($$) {
  if ($$.fragment !== null) {
    $$.update();
    run_all($$.before_update);
    const dirty = $$.dirty;
    $$.dirty = [-1];
    $$.fragment && $$.fragment.p($$.ctx, dirty);
    $$.after_update.forEach(add_render_callback);
  }
}
const generateTitle = async (token = "", template, model, prompt) => {
  let error = null;
  template = template.replace(/{{prompt}}/g, prompt);
  console.log(template);
  const res = await fetch(`${OLLAMA_API_BASE_URL}/api/generate`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify({
      model,
      prompt: template,
      stream: false
    })
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).catch((err) => {
    console.log(err);
    if ("detail" in err) {
      error = err.detail;
    }
    return null;
  });
  if (error) {
    throw error;
  }
  return res?.response ?? "New Chat";
};
const generateChatCompletion = async (token = "", body) => {
  let controller = new AbortController();
  let error = null;
  const res = await fetch(`${OLLAMA_API_BASE_URL}/api/chat`, {
    signal: controller.signal,
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(body)
  }).catch((err) => {
    error = err;
    return null;
  });
  if (error) {
    throw error;
  }
  return [res, controller];
};
const cancelChatCompletion = async (token = "", requestId) => {
  let error = null;
  const res = await fetch(`${OLLAMA_API_BASE_URL}/cancel/${requestId}`, {
    method: "GET",
    headers: {
      "Content-Type": "text/event-stream",
      Authorization: `Bearer ${token}`
    }
  }).catch((err) => {
    error = err;
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const createNewChat = async (token, chat) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/new`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      authorization: `Bearer ${token}`
    },
    body: JSON.stringify({
      chat
    })
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const getChatList = async (token = "") => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...token && { authorization: `Bearer ${token}` }
    }
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const getAllChatTags = async (token) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/tags/all`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...token && { authorization: `Bearer ${token}` }
    }
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const getChatById = async (token, id) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/${id}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...token && { authorization: `Bearer ${token}` }
    }
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const updateChatById = async (token, id, chat) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/${id}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...token && { authorization: `Bearer ${token}` }
    },
    body: JSON.stringify({
      chat
    })
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const getTagsById = async (token, id) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/${id}/tags`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...token && { authorization: `Bearer ${token}` }
    }
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const addTagById = async (token, id, tagName) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/${id}/tags`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...token && { authorization: `Bearer ${token}` }
    },
    body: JSON.stringify({
      tag_name: tagName,
      chat_id: id
    })
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const deleteTagById = async (token, id, tagName) => {
  let error = null;
  const res = await fetch(`${WEBUI_API_BASE_URL}/chats/${id}/tags`, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...token && { authorization: `Bearer ${token}` }
    },
    body: JSON.stringify({
      tag_name: tagName,
      chat_id: id
    })
  }).then(async (res2) => {
    if (!res2.ok)
      throw await res2.json();
    return res2.json();
  }).then((json) => {
    return json;
  }).catch((err) => {
    error = err;
    console.log(err);
    return null;
  });
  if (error) {
    throw error;
  }
  return res;
};
const splitStream = (splitOn) => {
  let buffer = "";
  return new TransformStream({
    transform(chunk, controller) {
      buffer += chunk;
      const parts = buffer.split(splitOn);
      parts.slice(0, -1).forEach((part) => controller.enqueue(part));
      buffer = parts[parts.length - 1];
    },
    flush(controller) {
      if (buffer)
        controller.enqueue(buffer);
    }
  });
};
const convertMessagesToHistory = (messages) => {
  const history = {
    messages: {},
    currentId: null
  };
  let parentMessageId = null;
  let messageId = null;
  for (const message of messages) {
    messageId = v4();
    if (parentMessageId !== null) {
      history.messages[parentMessageId].childrenIds = [
        ...history.messages[parentMessageId].childrenIds,
        messageId
      ];
    }
    history.messages[messageId] = {
      ...message,
      id: messageId,
      parentId: parentMessageId,
      childrenIds: []
    };
    parentMessageId = messageId;
  }
  history.currentId = messageId;
  return history;
};
const copyToClipboard = (text) => {
  if (!navigator.clipboard) {
    const textArea = document.createElement("textarea");
    textArea.value = text;
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();
    try {
      const successful = document.execCommand("copy");
      const msg = successful ? "successful" : "unsuccessful";
      console.log("Fallback: Copying text command was " + msg);
    } catch (err) {
      console.error("Fallback: Oops, unable to copy", err);
    }
    document.body.removeChild(textArea);
    return;
  }
  navigator.clipboard.writeText(text).then(
    function() {
      console.log("Async: Copying to clipboard was successful!");
    },
    function(err) {
      console.error("Async: Could not copy text: ", err);
    }
  );
};
const Tooltip = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { placement = "top" } = $$props;
  let { content = `I'm a tooltip!` } = $$props;
  let { touch = true } = $$props;
  let tooltipElement;
  onDestroy(() => {
  });
  if ($$props.placement === void 0 && $$bindings.placement && placement !== void 0)
    $$bindings.placement(placement);
  if ($$props.content === void 0 && $$bindings.content && content !== void 0)
    $$bindings.content(content);
  if ($$props.touch === void 0 && $$bindings.touch && touch !== void 0)
    $$bindings.touch(touch);
  return `<div${add_attribute("aria-label", content, 0)}${add_attribute("this", tooltipElement, 0)}>${slots.default ? slots.default({}) : ``}</div>`;
});
const PromptCommands = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $prompts, $$unsubscribe_prompts;
  $$unsubscribe_prompts = subscribe(prompts, (value) => $prompts = value);
  let { prompt = "" } = $$props;
  let selectedCommandIdx = 0;
  let filteredPromptCommands = [];
  const selectUp = () => {
    selectedCommandIdx = Math.max(0, selectedCommandIdx - 1);
  };
  const selectDown = () => {
    selectedCommandIdx = Math.min(selectedCommandIdx + 1, filteredPromptCommands.length - 1);
  };
  if ($$props.prompt === void 0 && $$bindings.prompt && prompt !== void 0)
    $$bindings.prompt(prompt);
  if ($$props.selectUp === void 0 && $$bindings.selectUp && selectUp !== void 0)
    $$bindings.selectUp(selectUp);
  if ($$props.selectDown === void 0 && $$bindings.selectDown && selectDown !== void 0)
    $$bindings.selectDown(selectDown);
  filteredPromptCommands = $prompts.filter((p) => p.command.includes(prompt)).sort((a, b) => a.title.localeCompare(b.title));
  {
    if (prompt) {
      selectedCommandIdx = 0;
    }
  }
  $$unsubscribe_prompts();
  return `${filteredPromptCommands.length > 0 ? `<div class="md:px-2 mb-3 text-left w-full absolute bottom-0 left-0 right-0"><div class="flex w-full px-2"><div class="bg-gray-100 dark:bg-gray-700 w-10 rounded-l-xl text-center" data-svelte-h="svelte-1ynv172"><div class="text-lg font-semibold mt-2">/</div></div> <div class="max-h-60 flex flex-col w-full rounded-r-xl bg-white"><div class="m-1 overflow-y-auto p-1 rounded-r-xl space-y-0.5">${each(filteredPromptCommands, (command, commandIdx) => {
    return `<button class="${"px-3 py-1.5 rounded-xl w-full text-left " + escape(
      commandIdx === selectedCommandIdx ? " bg-gray-100 selected-command-option-button" : "",
      true
    )}" type="button"><div class="font-medium text-black">${escape(command.command)}</div> <div class="text-xs text-gray-600">${escape(command.title)}</div> </button>`;
  })}</div> <div class="px-2 pb-1 text-xs text-gray-600 bg-white rounded-br-xl flex items-center space-x-1" data-svelte-h="svelte-rllwgv"><div><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3"><path stroke-linecap="round" stroke-linejoin="round" d="m11.25 11.25.041-.02a.75.75 0 0 1 1.063.852l-.708 2.836a.75.75 0 0 0 1.063.853l.041-.021M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Zm-9-3.75h.008v.008H12V8.25Z"></path></svg></div> <div class="line-clamp-1">Tip: Update multiple variable slots consecutively by pressing the tab key in the chat
						input after each replacement.</div></div></div></div></div>` : ``}`;
});
const Suggestions = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { submitPrompt } = $$props;
  let { suggestionPrompts = [] } = $$props;
  let prompts2 = [];
  if ($$props.submitPrompt === void 0 && $$bindings.submitPrompt && submitPrompt !== void 0)
    $$bindings.submitPrompt(submitPrompt);
  if ($$props.suggestionPrompts === void 0 && $$bindings.suggestionPrompts && suggestionPrompts !== void 0)
    $$bindings.suggestionPrompts(suggestionPrompts);
  prompts2 = suggestionPrompts.length <= 4 ? suggestionPrompts : suggestionPrompts.sort(() => Math.random() - 0.5).slice(0, 4);
  return `<div class="mb-3 md:p-1 text-left w-full"><div class="flex flex-wrap-reverse px-2 text-left">${each(prompts2, (prompt, promptIdx) => {
    return `<div class="${escape(promptIdx > 1 ? "hidden sm:inline-flex" : "", true) + " basis-full sm:basis-1/2 p-[5px] px-1"}"><button class="flex-1 flex justify-between w-full h-full px-4 py-2.5 bg-gray-50 hover:bg-gray-100 dark:bg-gray-850 dark:hover:bg-gray-800 rounded-2xl transition group"><div class="flex flex-col text-left self-center">${prompt.title && prompt.title[0] !== "" ? `<div class="text-sm font-medium dark:text-gray-300">${escape(prompt.title[0])}</div> <div class="text-sm text-gray-500 line-clamp-1">${escape(prompt.title[1])}</div>` : `<div class="self-center text-sm font-medium dark:text-gray-300 line-clamp-2">${escape(prompt.content)} </div>`}</div> <div class="self-center p-1 rounded-lg text-gray-50 group-hover:text-gray-800 dark:text-gray-850 dark:group-hover:text-gray-100 transition" data-svelte-h="svelte-qf8uca"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M8 14a.75.75 0 0 1-.75-.75V4.56L4.03 7.78a.75.75 0 0 1-1.06-1.06l4.5-4.5a.75.75 0 0 1 1.06 0l4.5 4.5a.75.75 0 0 1-1.06 1.06L8.75 4.56v8.69A.75.75 0 0 1 8 14Z" clip-rule="evenodd"></path></svg> </div></button> </div>`;
  })}</div></div>`;
});
const Documents = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let filteredCollections;
  let $documents, $$unsubscribe_documents;
  $$unsubscribe_documents = subscribe(documents, (value) => $documents = value);
  let { prompt = "" } = $$props;
  createEventDispatcher();
  let selectedIdx = 0;
  let filteredItems = [];
  let filteredDocs = [];
  let collections = [];
  const selectUp = () => {
    selectedIdx = Math.max(0, selectedIdx - 1);
  };
  const selectDown = () => {
    selectedIdx = Math.min(selectedIdx + 1, filteredItems.length - 1);
  };
  if ($$props.prompt === void 0 && $$bindings.prompt && prompt !== void 0)
    $$bindings.prompt(prompt);
  if ($$props.selectUp === void 0 && $$bindings.selectUp && selectUp !== void 0)
    $$bindings.selectUp(selectUp);
  if ($$props.selectDown === void 0 && $$bindings.selectDown && selectDown !== void 0)
    $$bindings.selectDown(selectDown);
  collections = [
    ...$documents.length > 0 ? [
      {
        name: "All Documents",
        type: "collection",
        title: "All Documents",
        collection_names: $documents.map((doc) => doc.collection_name)
      }
    ] : [],
    ...$documents.reduce(
      (a, e, i, arr) => {
        return [
          .../* @__PURE__ */ new Set([...a, ...(e?.content?.tags ?? []).map((tag) => tag.name)])
        ];
      },
      []
    ).map((tag) => ({
      name: tag,
      type: "collection",
      collection_names: $documents.filter((doc) => (doc?.content?.tags ?? []).map((tag2) => tag2.name).includes(tag)).map((doc) => doc.collection_name)
    }))
  ];
  filteredCollections = collections.filter((collection) => collection.name.includes(prompt.split(" ")?.at(0)?.substring(1) ?? "")).sort((a, b) => a.name.localeCompare(b.name));
  filteredDocs = $documents.filter((doc) => doc.name.includes(prompt.split(" ")?.at(0)?.substring(1) ?? "")).sort((a, b) => a.title.localeCompare(b.title));
  filteredItems = [...filteredCollections, ...filteredDocs];
  {
    if (prompt) {
      selectedIdx = 0;
      console.log(filteredCollections);
    }
  }
  $$unsubscribe_documents();
  return `${filteredItems.length > 0 || prompt.split(" ")?.at(0)?.substring(1).startsWith("http") ? `<div class="md:px-2 mb-3 text-left w-full absolute bottom-0 left-0 right-0"><div class="flex w-full px-2"><div class="bg-gray-100 dark:bg-gray-700 w-10 rounded-l-xl text-center" data-svelte-h="svelte-czbw16"><div class="text-lg font-semibold mt-2">#</div></div> <div class="max-h-60 flex flex-col w-full rounded-r-xl bg-white"><div class="m-1 overflow-y-auto p-1 rounded-r-xl space-y-0.5">${each(filteredItems, (doc, docIdx) => {
    return `<button class="${"px-3 py-1.5 rounded-xl w-full text-left " + escape(
      docIdx === selectedIdx ? " bg-gray-100 selected-command-option-button" : "",
      true
    )}" type="button">${doc.type === "collection" ? `<div class="font-medium text-black line-clamp-1">${escape(doc?.title ?? `#${doc.name}`)}</div> <div class="text-xs text-gray-600 line-clamp-1" data-svelte-h="svelte-6selaa">Collection</div>` : `<div class="font-medium text-black line-clamp-1">#${escape(doc.name)} (${escape(doc.filename)})</div> <div class="text-xs text-gray-600 line-clamp-1">${escape(doc.title)} </div>`} </button>`;
  })} ${prompt.split(" ")?.at(0)?.substring(1).startsWith("http") ? `<button class="px-3 py-1.5 rounded-xl w-full text-left bg-gray-100 selected-command-option-button" type="button"><div class="font-medium text-black line-clamp-1">${escape(prompt.split(" ")?.at(0)?.substring(1))}</div> <div class="text-xs text-gray-600 line-clamp-1" data-svelte-h="svelte-1l36nw6">Web</div></button>` : ``}</div></div></div></div>` : ``}`;
});
const Models = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $models, $$unsubscribe_models;
  $$unsubscribe_models = subscribe(models, (value) => $models = value);
  let { prompt = "" } = $$props;
  let { user: user2 = null } = $$props;
  let { chatInputPlaceholder = "" } = $$props;
  let { messages = [] } = $$props;
  let selectedIdx = 0;
  let filteredModels = [];
  const selectUp = () => {
    selectedIdx = Math.max(0, selectedIdx - 1);
  };
  const selectDown = () => {
    selectedIdx = Math.min(selectedIdx + 1, filteredModels.length - 1);
  };
  if ($$props.prompt === void 0 && $$bindings.prompt && prompt !== void 0)
    $$bindings.prompt(prompt);
  if ($$props.user === void 0 && $$bindings.user && user2 !== void 0)
    $$bindings.user(user2);
  if ($$props.chatInputPlaceholder === void 0 && $$bindings.chatInputPlaceholder && chatInputPlaceholder !== void 0)
    $$bindings.chatInputPlaceholder(chatInputPlaceholder);
  if ($$props.messages === void 0 && $$bindings.messages && messages !== void 0)
    $$bindings.messages(messages);
  if ($$props.selectUp === void 0 && $$bindings.selectUp && selectUp !== void 0)
    $$bindings.selectUp(selectUp);
  if ($$props.selectDown === void 0 && $$bindings.selectDown && selectDown !== void 0)
    $$bindings.selectDown(selectDown);
  filteredModels = $models.filter((p) => p.name !== "hr" && !p.external && p.name.includes(prompt.split(" ")?.at(0)?.substring(1) ?? "")).sort((a, b) => a.name.localeCompare(b.name));
  {
    if (prompt) {
      selectedIdx = 0;
    }
  }
  $$unsubscribe_models();
  return `${filteredModels.length > 0 ? `<div class="md:px-2 mb-3 text-left w-full absolute bottom-0 left-0 right-0"><div class="flex w-full px-2"><div class="bg-gray-100 dark:bg-gray-700 w-10 rounded-l-xl text-center" data-svelte-h="svelte-9l3i17"><div class="text-lg font-semibold mt-2">@</div></div> <div class="max-h-60 flex flex-col w-full rounded-r-xl bg-white"><div class="m-1 overflow-y-auto p-1 rounded-r-xl space-y-0.5">${each(filteredModels, (model, modelIdx) => {
    return `<button class="${"px-3 py-1.5 rounded-xl w-full text-left " + escape(
      modelIdx === selectedIdx ? " bg-gray-100 selected-command-option-button" : "",
      true
    )}" type="button"><div class="font-medium text-black line-clamp-1">${escape(model.name)}</div>  </button>`;
  })}</div></div></div></div>` : ``}`;
});
const MessageInput = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_settings;
  $$unsubscribe_settings = subscribe(settings, (value) => value);
  let { submitPrompt } = $$props;
  let { stopResponse } = $$props;
  let { suggestionPrompts = [] } = $$props;
  let { autoScroll = true } = $$props;
  let chatTextAreaElement;
  let promptsElement;
  let documentsElement;
  let modelsElement;
  let user2 = null;
  let chatInputPlaceholder = "";
  let { files = [] } = $$props;
  let { fileUploadEnabled = true } = $$props;
  let { speechRecognitionEnabled = true } = $$props;
  let { prompt = "" } = $$props;
  let { messages = [] } = $$props;
  if ($$props.submitPrompt === void 0 && $$bindings.submitPrompt && submitPrompt !== void 0)
    $$bindings.submitPrompt(submitPrompt);
  if ($$props.stopResponse === void 0 && $$bindings.stopResponse && stopResponse !== void 0)
    $$bindings.stopResponse(stopResponse);
  if ($$props.suggestionPrompts === void 0 && $$bindings.suggestionPrompts && suggestionPrompts !== void 0)
    $$bindings.suggestionPrompts(suggestionPrompts);
  if ($$props.autoScroll === void 0 && $$bindings.autoScroll && autoScroll !== void 0)
    $$bindings.autoScroll(autoScroll);
  if ($$props.files === void 0 && $$bindings.files && files !== void 0)
    $$bindings.files(files);
  if ($$props.fileUploadEnabled === void 0 && $$bindings.fileUploadEnabled && fileUploadEnabled !== void 0)
    $$bindings.fileUploadEnabled(fileUploadEnabled);
  if ($$props.speechRecognitionEnabled === void 0 && $$bindings.speechRecognitionEnabled && speechRecognitionEnabled !== void 0)
    $$bindings.speechRecognitionEnabled(speechRecognitionEnabled);
  if ($$props.prompt === void 0 && $$bindings.prompt && prompt !== void 0)
    $$bindings.prompt(prompt);
  if ($$props.messages === void 0 && $$bindings.messages && messages !== void 0)
    $$bindings.messages(messages);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    $$rendered = `${``} <div class="w-full"><div class="px-2.5 -mb-0.5 mx-auto inset-x-0 bg-transparent flex justify-center"><div class="flex flex-col max-w-3xl w-full"><div class="relative">${autoScroll === false && messages.length > 0 ? `<div class="absolute -top-12 left-0 right-0 flex justify-center"><button class="bg-white border border-gray-100 dark:border-none dark:bg-white/20 p-1.5 rounded-full" data-svelte-h="svelte-a2mahu"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path fill-rule="evenodd" d="M10 3a.75.75 0 01.75.75v10.638l3.96-4.158a.75.75 0 111.08 1.04l-5.25 5.5a.75.75 0 01-1.08 0l-5.25-5.5a.75.75 0 111.08-1.04l3.96 4.158V3.75A.75.75 0 0110 3z" clip-rule="evenodd"></path></svg></button></div>` : ``}</div> <div class="w-full relative">${prompt.charAt(0) === "/" ? `${validate_component(PromptCommands, "Prompts").$$render(
      $$result,
      { this: promptsElement, prompt },
      {
        this: ($$value) => {
          promptsElement = $$value;
          $$settled = false;
        },
        prompt: ($$value) => {
          prompt = $$value;
          $$settled = false;
        }
      },
      {}
    )}` : `${prompt.charAt(0) === "#" ? `${validate_component(Documents, "Documents").$$render(
      $$result,
      { this: documentsElement, prompt },
      {
        this: ($$value) => {
          documentsElement = $$value;
          $$settled = false;
        },
        prompt: ($$value) => {
          prompt = $$value;
          $$settled = false;
        }
      },
      {}
    )}` : `${prompt.charAt(0) === "@" ? `${validate_component(Models, "Models").$$render(
      $$result,
      {
        messages,
        this: modelsElement,
        prompt,
        user: user2,
        chatInputPlaceholder
      },
      {
        this: ($$value) => {
          modelsElement = $$value;
          $$settled = false;
        },
        prompt: ($$value) => {
          prompt = $$value;
          $$settled = false;
        },
        user: ($$value) => {
          user2 = $$value;
          $$settled = false;
        },
        chatInputPlaceholder: ($$value) => {
          chatInputPlaceholder = $$value;
          $$settled = false;
        }
      },
      {}
    )}` : ``}`}`} ${messages.length == 0 && suggestionPrompts.length !== 0 ? `${validate_component(Suggestions, "Suggestions").$$render($$result, { suggestionPrompts, submitPrompt }, {}, {})}` : ``}</div></div></div> <div class="bg-white/20 dark:bg-gray-900"><div class="max-w-3xl px-2.5 mx-auto inset-x-0"><div class="pb-2"><input type="file" hidden> <form class="flex flex-col relative w-full rounded-3xl px-1.5 border border-gray-100 dark:border-gray-850 bg-white dark:bg-gray-900 dark:text-gray-100">${files.length > 0 ? `<div class="mx-2 mt-2 mb-1 flex flex-wrap gap-2">${each(files, (file, fileIdx) => {
      return `<div class="relative group">${file.type === "image" ? `<img${add_attribute("src", file.url, 0)} alt="input" class="h-16 w-16 rounded-xl object-cover">` : `${file.type === "doc" ? `<div class="h-16 w-[15rem] flex items-center space-x-3 px-2.5 dark:bg-gray-600 rounded-xl border border-gray-200 dark:border-none"><div class="p-2.5 bg-red-400 text-white rounded-lg">${file.upload_status ? `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6"><path fill-rule="evenodd" d="M5.625 1.5c-1.036 0-1.875.84-1.875 1.875v17.25c0 1.035.84 1.875 1.875 1.875h12.75c1.035 0 1.875-.84 1.875-1.875V12.75A3.75 3.75 0 0 0 16.5 9h-1.875a1.875 1.875 0 0 1-1.875-1.875V5.25A3.75 3.75 0 0 0 9 1.5H5.625ZM7.5 15a.75.75 0 0 1 .75-.75h7.5a.75.75 0 0 1 0 1.5h-7.5A.75.75 0 0 1 7.5 15Zm.75 2.25a.75.75 0 0 0 0 1.5H12a.75.75 0 0 0 0-1.5H8.25Z" clip-rule="evenodd"></path><path d="M12.971 1.816A5.23 5.23 0 0 1 14.25 5.25v1.875c0 .207.168.375.375.375H16.5a5.23 5.23 0 0 1 3.434 1.279 9.768 9.768 0 0 0-6.963-6.963Z"></path></svg>` : `<svg class="w-6 h-6 translate-y-[0.5px]" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><style>.spinner_qM83 {
																animation: spinner_8HQG 1.05s infinite;
															}
															.spinner_oXPr {
																animation-delay: 0.1s;
															}
															.spinner_ZTLf {
																animation-delay: 0.2s;
															}
															@keyframes spinner_8HQG {
																0%,
																57.14% {
																	animation-timing-function: cubic-bezier(0.33, 0.66, 0.66, 1);
																	transform: translate(0);
																}
																28.57% {
																	animation-timing-function: cubic-bezier(0.33, 0, 0.66, 0.33);
																	transform: translateY(-6px);
																}
																100% {
																	transform: translate(0);
																}
															}
														</style><circle class="spinner_qM83" cx="4" cy="12" r="2.5"></circle><circle class="spinner_qM83 spinner_oXPr" cx="12" cy="12" r="2.5"></circle><circle class="spinner_qM83 spinner_ZTLf" cx="20" cy="12" r="2.5"></circle></svg>`}</div> <div class="flex flex-col justify-center -space-y-0.5"><div class="dark:text-gray-100 text-sm font-medium line-clamp-1">${escape(file.name)}</div> <div class="text-gray-500 text-sm" data-svelte-h="svelte-1xs684b">Document</div></div> </div>` : `${file.type === "collection" ? `<div class="h-16 w-[15rem] flex items-center space-x-3 px-2.5 dark:bg-gray-600 rounded-xl border border-gray-200 dark:border-none"><div class="p-2.5 bg-red-400 text-white rounded-lg" data-svelte-h="svelte-1h7831u"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6"><path d="M7.5 3.375c0-1.036.84-1.875 1.875-1.875h.375a3.75 3.75 0 0 1 3.75 3.75v1.875C13.5 8.161 14.34 9 15.375 9h1.875A3.75 3.75 0 0 1 21 12.75v3.375C21 17.16 20.16 18 19.125 18h-9.75A1.875 1.875 0 0 1 7.5 16.125V3.375Z"></path><path d="M15 5.25a5.23 5.23 0 0 0-1.279-3.434 9.768 9.768 0 0 1 6.963 6.963A5.23 5.23 0 0 0 17.25 7.5h-1.875A.375.375 0 0 1 15 7.125V5.25ZM4.875 6H6v10.125A3.375 3.375 0 0 0 9.375 19.5H16.5v1.125c0 1.035-.84 1.875-1.875 1.875h-9.75A1.875 1.875 0 0 1 3 20.625V7.875C3 6.839 3.84 6 4.875 6Z"></path></svg></div> <div class="flex flex-col justify-center -space-y-0.5"><div class="dark:text-gray-100 text-sm font-medium line-clamp-1">${escape(file?.title ?? `#${file.name}`)}</div> <div class="text-gray-500 text-sm" data-svelte-h="svelte-1jg2pem">Collection</div></div> </div>` : ``}`}`} <div class="absolute -top-1 -right-1"><button class="bg-gray-400 text-white border border-white rounded-full group-hover:visible invisible transition" type="button" data-svelte-h="svelte-3nr9d3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z"></path></svg> </button></div> </div>`;
    })}</div>` : ``} <div class="flex"> <textarea id="chat-textarea" class="${"dark:bg-gray-900 dark:text-gray-100 outline-none w-full py-3 px-3 " + escape(fileUploadEnabled ? "" : " pl-4", true) + " rounded-xl resize-none h-[48px]"}"${add_attribute(
      "placeholder",
      chatInputPlaceholder !== "" ? chatInputPlaceholder : "Send a message",
      0
    )} rows="1"${add_attribute("this", chatTextAreaElement, 0)}>${escape(prompt || "")}</textarea> <div class="self-end mb-2 flex space-x-1 mr-1">${messages.length == 0 || messages.at(-1).done == true ? `${validate_component(Tooltip, "Tooltip").$$render($$result, { content: "Record voice" }, {}, {
      default: () => {
        return `${speechRecognitionEnabled ? `<button id="voice-input-button" class="text-blue-600 dark:text-gray-300 hover:bg-blue-50 dark:hover:bg-gray-850 transition rounded-full p-1.5 mr-0.5 self-center" type="button">${`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5 translate-y-[0.5px]"><path d="M7 4a3 3 0 016 0v6a3 3 0 11-6 0V4z"></path><path d="M5.5 9.643a.75.75 0 00-1.5 0V10c0 3.06 2.29 5.585 5.25 5.954V17.5h-1.5a.75.75 0 000 1.5h4.5a.75.75 0 000-1.5h-1.5v-1.546A6.001 6.001 0 0016 10v-.357a.75.75 0 00-1.5 0V10a4.5 4.5 0 01-9 0v-.357z"></path></svg>`}</button>` : ``}`;
      }
    })} ${validate_component(Tooltip, "Tooltip").$$render($$result, { content: "Send message" }, {}, {
      default: () => {
        return `<button class="${escape(
          prompt !== "" ? "bg-blue-600 text-white hover:bg-blue-900 dark:bg-white dark:text-black dark:hover:bg-gray-100 " : "text-white bg-gray-100 dark:text-gray-900 dark:bg-gray-800 disabled",
          true
        ) + " transition rounded-full p-1.5 self-center"}" type="submit" ${prompt === "" ? "disabled" : ""}><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-5 h-5"><path fill-rule="evenodd" d="M8 14a.75.75 0 0 1-.75-.75V4.56L4.03 7.78a.75.75 0 0 1-1.06-1.06l4.5-4.5a.75.75 0 0 1 1.06 0l4.5 4.5a.75.75 0 0 1-1.06 1.06L8.75 4.56v8.69A.75.75 0 0 1 8 14Z" clip-rule="evenodd"></path></svg></button>`;
      }
    })}` : `<button class="bg-white hover:bg-gray-100 text-gray-800 dark:bg-gray-700 dark:text-white dark:hover:bg-gray-800 transition rounded-full p-1.5" data-svelte-h="svelte-tod56q"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-5 h-5"><path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm6-2.438c0-.724.588-1.312 1.313-1.312h4.874c.725 0 1.313.588 1.313 1.313v4.874c0 .725-.588 1.313-1.313 1.313H9.564a1.312 1.312 0 01-1.313-1.313V9.564z" clip-rule="evenodd"></path></svg></button>`}</div></div></form> <div class="mt-1.5 text-xs text-gray-500 text-center" data-svelte-h="svelte-1xl1sbd">LLMs can make mistakes. Verify important information.</div></div></div></div></div>`;
  } while (!$$settled);
  $$unsubscribe_settings();
  return $$rendered;
});
const Name = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `<div class="self-center font-bold mb-0.5 capitalize line-clamp-1">${slots.default ? slots.default({}) : ``}</div>`;
});
const ProfileImage = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { src = "/user.png" } = $$props;
  if ($$props.src === void 0 && $$bindings.src && src !== void 0)
    $$bindings.src(src);
  return `<div class="mr-4"><img${add_attribute("src", src, 0)} class="max-w-[28px] object-cover rounded-full" alt="profile" draggable="false"></div>`;
});
const UserMessage = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $modelfiles, $$unsubscribe_modelfiles;
  let $settings, $$unsubscribe_settings;
  $$unsubscribe_modelfiles = subscribe(modelfiles, (value) => $modelfiles = value);
  $$unsubscribe_settings = subscribe(settings, (value) => $settings = value);
  createEventDispatcher();
  let { user: user2 } = $$props;
  let { message } = $$props;
  let { siblings } = $$props;
  let { isFirstMessage } = $$props;
  let { confirmEditMessage } = $$props;
  let { showPreviousMessage } = $$props;
  let { showNextMessage } = $$props;
  let { copyToClipboard: copyToClipboard2 } = $$props;
  if ($$props.user === void 0 && $$bindings.user && user2 !== void 0)
    $$bindings.user(user2);
  if ($$props.message === void 0 && $$bindings.message && message !== void 0)
    $$bindings.message(message);
  if ($$props.siblings === void 0 && $$bindings.siblings && siblings !== void 0)
    $$bindings.siblings(siblings);
  if ($$props.isFirstMessage === void 0 && $$bindings.isFirstMessage && isFirstMessage !== void 0)
    $$bindings.isFirstMessage(isFirstMessage);
  if ($$props.confirmEditMessage === void 0 && $$bindings.confirmEditMessage && confirmEditMessage !== void 0)
    $$bindings.confirmEditMessage(confirmEditMessage);
  if ($$props.showPreviousMessage === void 0 && $$bindings.showPreviousMessage && showPreviousMessage !== void 0)
    $$bindings.showPreviousMessage(showPreviousMessage);
  if ($$props.showNextMessage === void 0 && $$bindings.showNextMessage && showNextMessage !== void 0)
    $$bindings.showNextMessage(showNextMessage);
  if ($$props.copyToClipboard === void 0 && $$bindings.copyToClipboard && copyToClipboard2 !== void 0)
    $$bindings.copyToClipboard(copyToClipboard2);
  $$unsubscribe_modelfiles();
  $$unsubscribe_settings();
  return `<div class="flex w-full">${validate_component(ProfileImage, "ProfileImage").$$render(
    $$result,
    {
      src: message.user ? $modelfiles.find((modelfile) => modelfile.tagName === message.user)?.imageUrl ?? "/user.png" : user2?.profile_image_url ?? "/user.png"
    },
    {},
    {}
  )} <div class="w-full overflow-hidden"><div class="user-message">${validate_component(Name, "Name").$$render($$result, {}, {}, {
    default: () => {
      return `${message.user ? `${$modelfiles.map((modelfile) => modelfile.tagName).includes(message.user) ? `${escape($modelfiles.find((modelfile) => modelfile.tagName === message.user)?.title)}` : `You <span class="text-gray-500 text-sm font-medium">${escape(message?.user ?? "")}</span>`}` : `${$settings.showUsername ? `${escape(user2.name)}` : `You`}`} ${message.timestamp ? `<span class="invisible group-hover:visible text-gray-400 text-xs font-medium">${escape(dayjs(message.timestamp * 1e3).format("DD/MM/YYYY HH:mm"))}</span>` : ``}`;
    }
  })}</div> <div class="${"prose chat-" + escape(message.role, true) + " w-full max-w-full dark:prose-invert prose-headings:my-0 prose-p:my-0 prose-p:-mb-4 prose-pre:my-0 prose-table:my-0 prose-blockquote:my-0 prose-img:my-0 prose-ul:-my-4 prose-ol:-my-4 prose-li:-my-3 prose-ul:-mb-6 prose-ol:-mb-6 prose-li:-mb-4 whitespace-pre-line"}">${message.files ? `<div class="my-2.5 w-full flex overflow-x-auto gap-2 flex-wrap">${each(message.files, (file) => {
    return `<div>${file.type === "image" ? `<img${add_attribute("src", file.url, 0)} alt="input" class="max-h-96 rounded-lg" draggable="false">` : `${file.type === "doc" ? `<button class="h-16 w-[15rem] flex items-center space-x-3 px-2.5 dark:bg-gray-600 rounded-xl border border-gray-200 dark:border-none text-left" type="button"><div class="p-2.5 bg-red-400 text-white rounded-lg" data-svelte-h="svelte-1tvrfqj"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6"><path fill-rule="evenodd" d="M5.625 1.5c-1.036 0-1.875.84-1.875 1.875v17.25c0 1.035.84 1.875 1.875 1.875h12.75c1.035 0 1.875-.84 1.875-1.875V12.75A3.75 3.75 0 0 0 16.5 9h-1.875a1.875 1.875 0 0 1-1.875-1.875V5.25A3.75 3.75 0 0 0 9 1.5H5.625ZM7.5 15a.75.75 0 0 1 .75-.75h7.5a.75.75 0 0 1 0 1.5h-7.5A.75.75 0 0 1 7.5 15Zm.75 2.25a.75.75 0 0 0 0 1.5H12a.75.75 0 0 0 0-1.5H8.25Z" clip-rule="evenodd"></path><path d="M12.971 1.816A5.23 5.23 0 0 1 14.25 5.25v1.875c0 .207.168.375.375.375H16.5a5.23 5.23 0 0 1 3.434 1.279 9.768 9.768 0 0 0-6.963-6.963Z"></path></svg></div> <div class="flex flex-col justify-center -space-y-0.5"><div class="dark:text-gray-100 text-sm font-medium line-clamp-1">${escape(file.name)}</div> <div class="text-gray-500 text-sm" data-svelte-h="svelte-1xs684b">Document</div> </div></button>  <button class="h-16 w-[15rem] flex items-center space-x-3 px-2.5 dark:bg-gray-600 rounded-xl border border-gray-200 dark:border-none text-left" type="button"><div class="p-2.5 bg-red-400 text-white rounded-lg" data-svelte-h="svelte-11vcjd6"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6"><path d="M7.5 3.375c0-1.036.84-1.875 1.875-1.875h.375a3.75 3.75 0 0 1 3.75 3.75v1.875C13.5 8.161 14.34 9 15.375 9h1.875A3.75 3.75 0 0 1 21 12.75v3.375C21 17.16 20.16 18 19.125 18h-9.75A1.875 1.875 0 0 1 7.5 16.125V3.375Z"></path><path d="M15 5.25a5.23 5.23 0 0 0-1.279-3.434 9.768 9.768 0 0 1 6.963 6.963A5.23 5.23 0 0 0 17.25 7.5h-1.875A.375.375 0 0 1 15 7.125V5.25ZM4.875 6H6v10.125A3.375 3.375 0 0 0 9.375 19.5H16.5v1.125c0 1.035-.84 1.875-1.875 1.875h-9.75A1.875 1.875 0 0 1 3 20.625V7.875C3 6.839 3.84 6 4.875 6Z"></path></svg></div> <div class="flex flex-col justify-center -space-y-0.5"><div class="dark:text-gray-100 text-sm font-medium line-clamp-1">${escape(file?.title ?? `#${file.name}`)}</div> <div class="text-gray-500 text-sm" data-svelte-h="svelte-1jg2pem">Collection</div></div> </button>` : ``}`} </div>`;
  })}</div>` : ``} ${`<div class="w-full"><pre id="user-message">${escape(message.content)}</pre> <div class="flex justify-start space-x-1 text-gray-700 dark:text-gray-500">${siblings.length > 1 ? `<div class="flex self-center"><button class="self-center dark:hover:text-white hover:text-black transition" data-svelte-h="svelte-3066u1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z" clip-rule="evenodd"></path></svg></button> <div class="text-xs font-bold self-center dark:text-gray-100">${escape(siblings.indexOf(message.id) + 1)} / ${escape(siblings.length)}</div> <button class="self-center dark:hover:text-white hover:text-black transition" data-svelte-h="svelte-1tm9wnt"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z" clip-rule="evenodd"></path></svg></button></div>` : ``} ${validate_component(Tooltip, "Tooltip").$$render($$result, { content: "Edit", placement: "bottom" }, {}, {
    default: () => {
      return `<button class="invisible group-hover:visible p-1 rounded dark:hover:text-white hover:text-black transition edit-user-message-button" data-svelte-h="svelte-1fyy3bm"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125"></path></svg></button>`;
    }
  })} ${validate_component(Tooltip, "Tooltip").$$render($$result, { content: "Copy", placement: "bottom" }, {}, {
    default: () => {
      return `<button class="invisible group-hover:visible p-1 rounded dark:hover:text-white hover:text-black transition" data-svelte-h="svelte-1fufle2"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M15.666 3.888A2.25 2.25 0 0013.5 2.25h-3c-1.03 0-1.9.693-2.166 1.638m7.332 0c.055.194.084.4.084.612v0a.75.75 0 01-.75.75H9a.75.75 0 01-.75-.75v0c0-.212.03-.418.084-.612m7.332 0c.646.049 1.288.11 1.927.184 1.1.128 1.907 1.077 1.907 2.185V19.5a2.25 2.25 0 01-2.25 2.25H6.75A2.25 2.25 0 014.5 19.5V6.257c0-1.108.806-2.057 1.907-2.185a48.208 48.208 0 011.927-.184"></path></svg></button>`;
    }
  })} ${!isFirstMessage ? `${validate_component(Tooltip, "Tooltip").$$render($$result, { content: "Delete", placement: "bottom" }, {}, {
    default: () => {
      return `<button class="invisible group-hover:visible p-1 rounded dark:hover:text-white hover:text-black transition" data-svelte-h="svelte-1r7mjym"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"></path></svg></button>`;
    }
  })}` : ``}</div></div>`}</div></div></div>`;
});
const katex_min = "";
const Skeleton = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `<div class="w-full mt-3" data-svelte-h="svelte-1wy3iiu"><div class="animate-pulse flex w-full"><div class="space-y-2 w-full"><div class="h-2 bg-gray-200 dark:bg-gray-600 rounded mr-14"></div> <div class="grid grid-cols-3 gap-4"><div class="h-2 bg-gray-200 dark:bg-gray-600 rounded col-span-2"></div> <div class="h-2 bg-gray-200 dark:bg-gray-600 rounded col-span-1"></div></div> <div class="grid grid-cols-4 gap-4"><div class="h-2 bg-gray-200 dark:bg-gray-600 rounded col-span-1"></div> <div class="h-2 bg-gray-200 dark:bg-gray-600 rounded col-span-2"></div> <div class="h-2 bg-gray-200 dark:bg-gray-600 rounded col-span-1 mr-4"></div></div> <div class="h-2 bg-gray-200 dark:bg-gray-600 rounded"></div></div></div></div>`;
});
const githubDark_min = "";
const CodeBlock = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let highlightedCode;
  let { lang = "" } = $$props;
  let { code = "" } = $$props;
  if ($$props.lang === void 0 && $$bindings.lang && lang !== void 0)
    $$bindings.lang(lang);
  if ($$props.code === void 0 && $$bindings.code && code !== void 0)
    $$bindings.code(code);
  highlightedCode = code ? hljs.highlightAuto(code, hljs.getLanguage(lang)?.aliases).value : "";
  return `${code ? `<div class="mb-4"><div class="flex justify-between bg-[#202123] text-white text-xs px-4 pt-1 pb-0.5 rounded-t-lg overflow-x-auto"><div class="p-1"><!-- HTML_TAG_START -->${lang}<!-- HTML_TAG_END --></div> <button class="copy-code-button bg-none border-none p-1">${escape("Copy Code")}</button></div> <pre class="rounded-b-lg hljs p-4 px-5 overflow-x-auto rounded-t-none"><code class="${"language-" + escape(lang, true) + " rounded-t-none whitespace-pre"}"><!-- HTML_TAG_START -->${highlightedCode || code}<!-- HTML_TAG_END --></code></pre></div>` : ``}`;
});
const ImagePreview = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { show = false } = $$props;
  let { src = "" } = $$props;
  let { alt = "" } = $$props;
  if ($$props.show === void 0 && $$bindings.show && show !== void 0)
    $$bindings.show(show);
  if ($$props.src === void 0 && $$bindings.src && src !== void 0)
    $$bindings.src(src);
  if ($$props.alt === void 0 && $$bindings.alt && alt !== void 0)
    $$bindings.alt(alt);
  return `${show ? `  <div class="fixed top-0 right-0 left-0 bottom-0 bg-black text-white w-full min-h-screen h-screen flex justify-center z-50 overflow-hidden overscroll-contain"><div class="absolute left-0 w-full flex justify-between"><div><button class="p-5" data-svelte-h="svelte-1klk9rz"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-6 h-6"><path stroke-linecap="round" stroke-linejoin="round" d="M6 18 18 6M6 6l12 12"></path></svg></button></div> <div><button class="p-5" data-svelte-h="svelte-1kr0piu"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-6 h-6"><path d="M10.75 2.75a.75.75 0 0 0-1.5 0v8.614L6.295 8.235a.75.75 0 1 0-1.09 1.03l4.25 4.5a.75.75 0 0 0 1.09 0l4.25-4.5a.75.75 0 0 0-1.09-1.03l-2.955 3.129V2.75Z"></path><path d="M3.5 12.75a.75.75 0 0 0-1.5 0v2.5A2.75 2.75 0 0 0 4.75 18h10.5A2.75 2.75 0 0 0 18 15.25v-2.5a.75.75 0 0 0-1.5 0v2.5c0 .69-.56 1.25-1.25 1.25H4.75c-.69 0-1.25-.56-1.25-1.25v-2.5Z"></path></svg></button></div></div> <img${add_attribute("src", src, 0)}${add_attribute("alt", alt, 0)} class="mx-auto h-full object-scale-down"></div>` : ``}`;
});
const Image = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let { src = "" } = $$props;
  let { alt = "" } = $$props;
  let _src = "";
  let showImagePreview = false;
  if ($$props.src === void 0 && $$bindings.src && src !== void 0)
    $$bindings.src(src);
  if ($$props.alt === void 0 && $$bindings.alt && alt !== void 0)
    $$bindings.alt(alt);
  let $$settled;
  let $$rendered;
  let previous_head = $$result.head;
  do {
    $$settled = true;
    $$result.head = previous_head;
    _src = src.startsWith("/") ? `${WEBUI_BASE_URL}${src}` : src;
    $$rendered = `${validate_component(ImagePreview, "ImagePreview").$$render(
      $$result,
      { src: _src, alt, show: showImagePreview },
      {
        show: ($$value) => {
          showImagePreview = $$value;
          $$settled = false;
        }
      },
      {}
    )} <button><img${add_attribute("src", _src, 0)}${add_attribute("alt", alt, 0)} class="max-h-96 rounded-lg" draggable="false"></button>`;
  } while (!$$settled);
  return $$rendered;
});
const ResponseMessage_svelte_svelte_type_style_lang = "";
const css = {
  code: ".buttons.svelte-1u5gq5j::-webkit-scrollbar{display:none}.buttons.svelte-1u5gq5j{-ms-overflow-style:none;scrollbar-width:none}",
  map: null
};
const ResponseMessage = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let tokens;
  let $$unsubscribe_settings;
  let $config, $$unsubscribe_config;
  $$unsubscribe_settings = subscribe(settings, (value) => value);
  $$unsubscribe_config = subscribe(config, (value) => $config = value);
  createEventDispatcher();
  let { modelfiles: modelfiles2 = [] } = $$props;
  let { message } = $$props;
  let { siblings } = $$props;
  let { isLastMessage = true } = $$props;
  let { confirmEditResponseMessage } = $$props;
  let { showPreviousMessage } = $$props;
  let { showNextMessage } = $$props;
  let { rateMessage } = $$props;
  let { copyToClipboard: copyToClipboard2 } = $$props;
  let { continueGeneration } = $$props;
  let { regenerateResponse } = $$props;
  let tooltipInstance = null;
  const renderer = new marked.Renderer();
  renderer.codespan = (code) => {
    return `<code>${code.replaceAll("&amp;", "&")}</code>`;
  };
  const { extensions, ...defaults } = marked.getDefaults();
  const renderStyling = async () => {
    await tick();
    if (tooltipInstance) {
      tooltipInstance[0]?.destroy();
    }
    renderLatex();
    if (message.info) {
      tooltipInstance = tippy(`#info-${message.id}`, {
        content: `<span class="text-xs" id="tooltip-${message.id}">response_token/s: ${`${Math.round((message.info.eval_count ?? 0) / (message.info.eval_duration / 1e9) * 100) / 100} tokens` ?? "N/A"}<br/>
					prompt_token/s: ${Math.round((message.info.prompt_eval_count ?? 0) / (message.info.prompt_eval_duration / 1e9) * 100) / 100} tokens<br/>
                    total_duration: ${Math.round((message.info.total_duration ?? 0) / 1e6 * 100) / 100}ms<br/>
                    load_duration: ${Math.round((message.info.load_duration ?? 0) / 1e6 * 100) / 100}ms<br/>
                    prompt_eval_count: ${message.info.prompt_eval_count ?? "N/A"}<br/>
                    prompt_eval_duration: ${Math.round((message.info.prompt_eval_duration ?? 0) / 1e6 * 100) / 100}ms<br/>
                    eval_count: ${message.info.eval_count ?? "N/A"}<br/>
                    eval_duration: ${Math.round((message.info.eval_duration ?? 0) / 1e6 * 100) / 100}ms</span>`,
        allowHTML: true
      });
    }
  };
  const renderLatex = () => {
    let chatMessageElements = document.getElementsByClassName("chat-assistant");
    for (const element of chatMessageElements) {
      auto_render(element, {
        // customised options
        // • auto-render specific keys, e.g.:
        delimiters: [
          { left: "$$", right: "$$", display: false },
          { left: "$", right: "$", display: false },
          {
            left: "\\(",
            right: "\\)",
            display: false
          },
          {
            left: "\\[",
            right: "\\]",
            display: false
          },
          { left: "[ ", right: " ]", display: false }
        ],
        // • rendering keys, e.g.:
        throwOnError: false
      });
    }
  };
  if ($$props.modelfiles === void 0 && $$bindings.modelfiles && modelfiles2 !== void 0)
    $$bindings.modelfiles(modelfiles2);
  if ($$props.message === void 0 && $$bindings.message && message !== void 0)
    $$bindings.message(message);
  if ($$props.siblings === void 0 && $$bindings.siblings && siblings !== void 0)
    $$bindings.siblings(siblings);
  if ($$props.isLastMessage === void 0 && $$bindings.isLastMessage && isLastMessage !== void 0)
    $$bindings.isLastMessage(isLastMessage);
  if ($$props.confirmEditResponseMessage === void 0 && $$bindings.confirmEditResponseMessage && confirmEditResponseMessage !== void 0)
    $$bindings.confirmEditResponseMessage(confirmEditResponseMessage);
  if ($$props.showPreviousMessage === void 0 && $$bindings.showPreviousMessage && showPreviousMessage !== void 0)
    $$bindings.showPreviousMessage(showPreviousMessage);
  if ($$props.showNextMessage === void 0 && $$bindings.showNextMessage && showNextMessage !== void 0)
    $$bindings.showNextMessage(showNextMessage);
  if ($$props.rateMessage === void 0 && $$bindings.rateMessage && rateMessage !== void 0)
    $$bindings.rateMessage(rateMessage);
  if ($$props.copyToClipboard === void 0 && $$bindings.copyToClipboard && copyToClipboard2 !== void 0)
    $$bindings.copyToClipboard(copyToClipboard2);
  if ($$props.continueGeneration === void 0 && $$bindings.continueGeneration && continueGeneration !== void 0)
    $$bindings.continueGeneration(continueGeneration);
  if ($$props.regenerateResponse === void 0 && $$bindings.regenerateResponse && regenerateResponse !== void 0)
    $$bindings.regenerateResponse(regenerateResponse);
  $$result.css.add(css);
  tokens = marked.lexer(message.content);
  {
    if (message) {
      renderStyling();
    }
  }
  $$unsubscribe_settings();
  $$unsubscribe_config();
  return `<div class="${"flex w-full message-" + escape(message.id, true) + " svelte-1u5gq5j"}">${validate_component(ProfileImage, "ProfileImage").$$render(
    $$result,
    {
      src: modelfiles2[message.model]?.imageUrl ?? `${WEBUI_BASE_URL}/static/favicon.png`
    },
    {},
    {}
  )} <div class="w-full overflow-hidden">${validate_component(Name, "Name").$$render($$result, {}, {}, {
    default: () => {
      return `${message.model in modelfiles2 ? `${escape(modelfiles2[message.model]?.title)}` : `${escape(message.model ? ` ${message.model}` : "")}`} ${message.timestamp ? `<span class="invisible group-hover:visible text-gray-400 text-xs font-medium">${escape(dayjs(message.timestamp * 1e3).format("DD/MM/YYYY HH:mm"))}</span>` : ``}`;
    }
  })} ${message.content === "" ? `${validate_component(Skeleton, "Skeleton").$$render($$result, {}, {}, {})}` : `${message.files ? `<div class="my-2.5 w-full flex overflow-x-auto gap-2 flex-wrap">${each(message.files, (file) => {
    return `<div>${file.type === "image" ? `${validate_component(Image, "Image").$$render($$result, { src: file.url }, {}, {})}` : ``} </div>`;
  })}</div>` : ``} <div class="${"prose chat-" + escape(message.role, true) + " w-full max-w-full dark:prose-invert prose-headings:my-0 prose-p:m-0 prose-p:-mb-6 prose-pre:my-0 prose-table:my-0 prose-blockquote:my-0 prose-img:my-0 prose-ul:-my-4 prose-ol:-my-4 prose-li:-my-3 prose-ul:-mb-6 prose-ol:-mb-8 prose-ol:p-0 prose-li:-mb-4 whitespace-pre-line svelte-1u5gq5j"}"><div>${`<div class="w-full">${message?.error === true ? `<div class="flex mt-2 mb-4 space-x-2 border px-4 py-3 border-red-800 bg-red-800/30 font-medium rounded-lg"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 self-center"><path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z"></path></svg> <div class="self-center">${escape(message.content)}</div></div>` : `${each(tokens, (token) => {
    return `${token.type === "code" ? ` ${validate_component(CodeBlock, "CodeBlock").$$render($$result, { lang: token.lang, code: token.text }, {}, {})}` : `<!-- HTML_TAG_START -->${marked.parse(token.raw, {
      ...defaults,
      gfm: true,
      breaks: true,
      renderer
    })}<!-- HTML_TAG_END -->`}`;
  })} `} ${message.done ? `<div class="flex justify-start space-x-1 overflow-x-auto buttons text-gray-700 dark:text-gray-500 svelte-1u5gq5j">${siblings.length > 1 ? `<div class="flex self-center min-w-fit"><button class="self-center dark:hover:text-white hover:text-black transition" data-svelte-h="svelte-5exxlt"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z" clip-rule="evenodd"></path></svg></button> <div class="text-xs font-bold self-center min-w-fit dark:text-gray-100">${escape(siblings.indexOf(message.id) + 1)} / ${escape(siblings.length)}</div> <button class="self-center dark:hover:text-white hover:text-black transition" data-svelte-h="svelte-m0kbu1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z" clip-rule="evenodd"></path></svg></button></div>` : ``} ${validate_component(Tooltip, "Tooltip").$$render($$result, { content: "Edit", placement: "bottom" }, {}, {
    default: () => {
      return `<button class="${escape(
        isLastMessage ? "visible" : "invisible group-hover:visible",
        true
      ) + " p-1 rounded dark:hover:text-white hover:text-black transition"}"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125"></path></svg></button>`;
    }
  })} ${validate_component(Tooltip, "Tooltip").$$render($$result, { content: "Copy", placement: "bottom" }, {}, {
    default: () => {
      return `<button class="${escape(
        isLastMessage ? "visible" : "invisible group-hover:visible",
        true
      ) + " p-1 rounded dark:hover:text-white hover:text-black transition copy-response-button"}"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M15.666 3.888A2.25 2.25 0 0013.5 2.25h-3c-1.03 0-1.9.693-2.166 1.638m7.332 0c.055.194.084.4.084.612v0a.75.75 0 01-.75.75H9a.75.75 0 01-.75-.75v0c0-.212.03-.418.084-.612m7.332 0c.646.049 1.288.11 1.927.184 1.1.128 1.907 1.077 1.907 2.185V19.5a2.25 2.25 0 01-2.25 2.25H6.75A2.25 2.25 0 014.5 19.5V6.257c0-1.108.806-2.057 1.907-2.185a48.208 48.208 0 011.927-.184"></path></svg></button>`;
    }
  })} ${validate_component(Tooltip, "Tooltip").$$render(
    $$result,
    {
      content: "Good Response",
      placement: "bottom"
    },
    {},
    {
      default: () => {
        return `<button class="${escape(
          isLastMessage ? "visible" : "invisible group-hover:visible",
          true
        ) + " p-1 rounded " + escape(
          message.rating === 1 ? "bg-gray-100 dark:bg-gray-800" : "",
          true
        ) + " dark:hover:text-white hover:text-black transition"}"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="w-4 h-4" xmlns="http://www.w3.org/2000/svg"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg></button>`;
      }
    }
  )} ${validate_component(Tooltip, "Tooltip").$$render(
    $$result,
    {
      content: "Bad Response",
      placement: "bottom"
    },
    {},
    {
      default: () => {
        return `<button class="${escape(
          isLastMessage ? "visible" : "invisible group-hover:visible",
          true
        ) + " p-1 rounded " + escape(
          message.rating === -1 ? "bg-gray-100 dark:bg-gray-800" : "",
          true
        ) + " dark:hover:text-white hover:text-black transition"}"><svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" class="w-4 h-4" xmlns="http://www.w3.org/2000/svg"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></button>`;
      }
    }
  )} ${validate_component(Tooltip, "Tooltip").$$render(
    $$result,
    {
      content: "Read Aloud",
      placement: "bottom"
    },
    {},
    {
      default: () => {
        return `<button id="${"speak-button-" + escape(message.id, true)}" class="${escape(
          isLastMessage ? "visible" : "invisible group-hover:visible",
          true
        ) + " p-1 rounded dark:hover:text-white hover:text-black transition"}">${`${`<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M19.114 5.636a9 9 0 010 12.728M16.463 8.288a5.25 5.25 0 010 7.424M6.75 8.25l4.72-4.72a.75.75 0 011.28.53v15.88a.75.75 0 01-1.28.53l-4.72-4.72H4.51c-.88 0-1.704-.507-1.938-1.354A9.01 9.01 0 012.25 12c0-.83.112-1.633.322-2.396C2.806 8.756 3.63 8.25 4.51 8.25H6.75z"></path></svg>`}`}</button>`;
      }
    }
  )} ${$config.images ? `${validate_component(Tooltip, "Tooltip").$$render(
    $$result,
    {
      content: "Generate Image",
      placement: "bottom"
    },
    {},
    {
      default: () => {
        return `<button class="${escape(
          isLastMessage ? "visible" : "invisible group-hover:visible",
          true
        ) + " p-1 rounded dark:hover:text-white hover:text-black transition"}">${`<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="m2.25 15.75 5.159-5.159a2.25 2.25 0 0 1 3.182 0l5.159 5.159m-1.5-1.5 1.409-1.409a2.25 2.25 0 0 1 3.182 0l2.909 2.909m-18 3.75h16.5a1.5 1.5 0 0 0 1.5-1.5V6a1.5 1.5 0 0 0-1.5-1.5H3.75A1.5 1.5 0 0 0 2.25 6v12a1.5 1.5 0 0 0 1.5 1.5Zm10.5-11.25h.008v.008h-.008V8.25Zm.375 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Z"></path></svg>`}</button>`;
      }
    }
  )}` : ``} ${message.info ? `${validate_component(Tooltip, "Tooltip").$$render(
    $$result,
    {
      content: "Generation Info",
      placement: "bottom"
    },
    {},
    {
      default: () => {
        return `<button class="${"" + escape(
          isLastMessage ? "visible" : "invisible group-hover:visible",
          true
        ) + " p-1 rounded dark:hover:text-white hover:text-black transition whitespace-pre-wrap"}" id="${"info-" + escape(message.id, true)}"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"></path></svg></button>`;
      }
    }
  )}` : ``} ${isLastMessage ? `${validate_component(Tooltip, "Tooltip").$$render(
    $$result,
    {
      content: "Continue Response",
      placement: "bottom"
    },
    {},
    {
      default: () => {
        return `<button type="button" class="${escape(
          isLastMessage ? "visible" : "invisible group-hover:visible",
          true
        ) + " p-1 rounded dark:hover:text-white hover:text-black transition regenerate-response-button"}"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"></path><path stroke-linecap="round" stroke-linejoin="round" d="M15.91 11.672a.375.375 0 0 1 0 .656l-5.603 3.113a.375.375 0 0 1-.557-.328V8.887c0-.286.307-.466.557-.327l5.603 3.112Z"></path></svg></button>`;
      }
    }
  )} ${validate_component(Tooltip, "Tooltip").$$render(
    $$result,
    {
      content: "Regenerate",
      placement: "bottom"
    },
    {},
    {
      default: () => {
        return `<button type="button" class="${escape(
          isLastMessage ? "visible" : "invisible group-hover:visible",
          true
        ) + " p-1 rounded dark:hover:text-white hover:text-black transition regenerate-response-button"}"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" class="w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"></path></svg></button>`;
      }
    }
  )}` : ``}</div>` : ``}</div>`}</div></div>`}</div></div>`;
});
const Placeholder = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $user, $$unsubscribe_user;
  $$unsubscribe_user = subscribe(user, (value) => $user = value);
  let { models: models2 = [] } = $$props;
  let { modelfiles: modelfiles2 = [] } = $$props;
  let modelfile = null;
  let selectedModelIdx = 0;
  if ($$props.models === void 0 && $$bindings.models && models2 !== void 0)
    $$bindings.models(models2);
  if ($$props.modelfiles === void 0 && $$bindings.modelfiles && modelfiles2 !== void 0)
    $$bindings.modelfiles(modelfiles2);
  {
    if (models2.length > 0) {
      selectedModelIdx = models2.length - 1;
    }
  }
  modelfile = models2[selectedModelIdx] in modelfiles2 ? modelfiles2[models2[selectedModelIdx]] : null;
  $$unsubscribe_user();
  return `${models2.length > 0 ? `<div class="m-auto text-center max-w-md px-2"><div class="flex justify-center mt-8"><div class="flex -space-x-4 mb-1">${each(models2, (model, modelIdx) => {
    return `<button>${model in modelfiles2 ? `<img${add_attribute("src", modelfiles2[model]?.imageUrl ?? `${WEBUI_BASE_URL}/static/favicon.png`, 0)} alt="modelfile" class="w-14 rounded-full border-[1px] border-gray-200 dark:border-none" draggable="false">` : `<img${add_attribute(
      "src",
      models2.length === 1 ? `${WEBUI_BASE_URL}/static/favicon.png` : `${WEBUI_BASE_URL}/static/favicon.png`,
      0
    )} class="w-14 rounded-full border-[1px] border-gray-200 dark:border-none" alt="logo" draggable="false">`} </button>`;
  })}</div></div> <div class="mt-2 mb-5 text-2xl text-gray-800 dark:text-gray-100 font-semibold">${modelfile ? `<span class="capitalize">${escape(modelfile.title)}</span> <div class="mt-0.5 text-base font-normal text-gray-600 dark:text-gray-400">${escape(modelfile.desc)}</div> ${modelfile.user ? `<div class="mt-0.5 text-sm font-normal text-gray-500 dark:text-gray-500">By <a href="${"https://openwebui.com/m/" + escape(modelfile.user.username, true)}">${escape(modelfile.user.name ? modelfile.user.name : `@${modelfile.user.username}`)}</a></div>` : ``}` : `<div class="line-clamp-1">Hello, ${escape($user.name)}</div> <div data-svelte-h="svelte-1s1nbzv">How can I help you today?</div>`}</div></div>` : ``}`;
});
const Messages = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $settings, $$unsubscribe_settings;
  let $user, $$unsubscribe_user;
  $$unsubscribe_settings = subscribe(settings, (value) => $settings = value);
  $$unsubscribe_user = subscribe(user, (value) => $user = value);
  let { chatId = "" } = $$props;
  let { sendPrompt } = $$props;
  let { continueGeneration } = $$props;
  let { regenerateResponse } = $$props;
  let { processing = "" } = $$props;
  let { bottomPadding = false } = $$props;
  let { autoScroll } = $$props;
  let { selectedModels } = $$props;
  let { history = {} } = $$props;
  let { messages = [] } = $$props;
  let { selectedModelfiles = [] } = $$props;
  const scrollToBottom = () => {
    const element = document.getElementById("messages-container");
    element.scrollTop = element.scrollHeight;
  };
  const copyToClipboard2 = (text) => {
    if (!navigator.clipboard) {
      var textArea = document.createElement("textarea");
      textArea.value = text;
      textArea.style.top = "0";
      textArea.style.left = "0";
      textArea.style.position = "fixed";
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();
      try {
        var successful = document.execCommand("copy");
        var msg = successful ? "successful" : "unsuccessful";
        console.log("Fallback: Copying text command was " + msg);
      } catch (err) {
        console.error("Fallback: Oops, unable to copy", err);
      }
      document.body.removeChild(textArea);
      return;
    }
    navigator.clipboard.writeText(text).then(
      function() {
        console.log("Async: Copying to clipboard was successful!");
        toast.success("Copying to clipboard was successful!");
      },
      function(err) {
        console.error("Async: Could not copy text: ", err);
      }
    );
  };
  const confirmEditMessage = async (messageId, content) => {
    let userPrompt = content;
    let userMessageId = v4();
    let userMessage = {
      id: userMessageId,
      parentId: history.messages[messageId].parentId,
      childrenIds: [],
      role: "user",
      content: userPrompt,
      ...history.messages[messageId].files && { files: history.messages[messageId].files }
    };
    let messageParentId = history.messages[messageId].parentId;
    if (messageParentId !== null) {
      history.messages[messageParentId].childrenIds = [...history.messages[messageParentId].childrenIds, userMessageId];
    }
    history.messages[userMessageId] = userMessage;
    history.currentId = userMessageId;
    await tick();
    await sendPrompt(userPrompt, userMessageId, chatId);
  };
  const confirmEditResponseMessage = async (messageId, content) => {
    history.messages[messageId].originalContent = history.messages[messageId].content;
    history.messages[messageId].content = content;
    await tick();
    await updateChatById(localStorage.token, chatId, { messages, history });
    await chats.set(await getChatList(localStorage.token));
  };
  const rateMessage = async (messageId, rating) => {
    history.messages[messageId].rating = rating;
    await tick();
    await updateChatById(localStorage.token, chatId, { messages, history });
    await chats.set(await getChatList(localStorage.token));
  };
  const showPreviousMessage = async (message) => {
    if (message.parentId !== null) {
      let messageId = history.messages[message.parentId].childrenIds[Math.max(history.messages[message.parentId].childrenIds.indexOf(message.id) - 1, 0)];
      if (message.id !== messageId) {
        let messageChildrenIds = history.messages[messageId].childrenIds;
        while (messageChildrenIds.length !== 0) {
          messageId = messageChildrenIds.at(-1);
          messageChildrenIds = history.messages[messageId].childrenIds;
        }
        history.currentId = messageId;
      }
    } else {
      let childrenIds = Object.values(history.messages).filter((message2) => message2.parentId === null).map((message2) => message2.id);
      let messageId = childrenIds[Math.max(childrenIds.indexOf(message.id) - 1, 0)];
      if (message.id !== messageId) {
        let messageChildrenIds = history.messages[messageId].childrenIds;
        while (messageChildrenIds.length !== 0) {
          messageId = messageChildrenIds.at(-1);
          messageChildrenIds = history.messages[messageId].childrenIds;
        }
        history.currentId = messageId;
      }
    }
    await tick();
    const element = document.getElementById("messages-container");
    autoScroll = element.scrollHeight - element.scrollTop <= element.clientHeight + 50;
    setTimeout(
      () => {
        scrollToBottom();
      },
      100
    );
  };
  const showNextMessage = async (message) => {
    if (message.parentId !== null) {
      let messageId = history.messages[message.parentId].childrenIds[Math.min(history.messages[message.parentId].childrenIds.indexOf(message.id) + 1, history.messages[message.parentId].childrenIds.length - 1)];
      if (message.id !== messageId) {
        let messageChildrenIds = history.messages[messageId].childrenIds;
        while (messageChildrenIds.length !== 0) {
          messageId = messageChildrenIds.at(-1);
          messageChildrenIds = history.messages[messageId].childrenIds;
        }
        history.currentId = messageId;
      }
    } else {
      let childrenIds = Object.values(history.messages).filter((message2) => message2.parentId === null).map((message2) => message2.id);
      let messageId = childrenIds[Math.min(childrenIds.indexOf(message.id) + 1, childrenIds.length - 1)];
      if (message.id !== messageId) {
        let messageChildrenIds = history.messages[messageId].childrenIds;
        while (messageChildrenIds.length !== 0) {
          messageId = messageChildrenIds.at(-1);
          messageChildrenIds = history.messages[messageId].childrenIds;
        }
        history.currentId = messageId;
      }
    }
    await tick();
    const element = document.getElementById("messages-container");
    autoScroll = element.scrollHeight - element.scrollTop <= element.clientHeight + 50;
    setTimeout(
      () => {
        scrollToBottom();
      },
      100
    );
  };
  if ($$props.chatId === void 0 && $$bindings.chatId && chatId !== void 0)
    $$bindings.chatId(chatId);
  if ($$props.sendPrompt === void 0 && $$bindings.sendPrompt && sendPrompt !== void 0)
    $$bindings.sendPrompt(sendPrompt);
  if ($$props.continueGeneration === void 0 && $$bindings.continueGeneration && continueGeneration !== void 0)
    $$bindings.continueGeneration(continueGeneration);
  if ($$props.regenerateResponse === void 0 && $$bindings.regenerateResponse && regenerateResponse !== void 0)
    $$bindings.regenerateResponse(regenerateResponse);
  if ($$props.processing === void 0 && $$bindings.processing && processing !== void 0)
    $$bindings.processing(processing);
  if ($$props.bottomPadding === void 0 && $$bindings.bottomPadding && bottomPadding !== void 0)
    $$bindings.bottomPadding(bottomPadding);
  if ($$props.autoScroll === void 0 && $$bindings.autoScroll && autoScroll !== void 0)
    $$bindings.autoScroll(autoScroll);
  if ($$props.selectedModels === void 0 && $$bindings.selectedModels && selectedModels !== void 0)
    $$bindings.selectedModels(selectedModels);
  if ($$props.history === void 0 && $$bindings.history && history !== void 0)
    $$bindings.history(history);
  if ($$props.messages === void 0 && $$bindings.messages && messages !== void 0)
    $$bindings.messages(messages);
  if ($$props.selectedModelfiles === void 0 && $$bindings.selectedModelfiles && selectedModelfiles !== void 0)
    $$bindings.selectedModelfiles(selectedModelfiles);
  {
    if (autoScroll && bottomPadding) {
      (async () => {
        await tick();
        scrollToBottom();
      })();
    }
  }
  $$unsubscribe_settings();
  $$unsubscribe_user();
  return `${messages.length == 0 ? `${validate_component(Placeholder, "Placeholder").$$render(
    $$result,
    {
      models: selectedModels,
      modelfiles: selectedModelfiles
    },
    {},
    {}
  )}` : `<div class="pb-10">${each(messages, (message, messageIdx) => {
    return `<div class="w-full"><div class="${"flex flex-col justify-between px-5 mb-3 " + escape(
      $settings?.fullScreenMode ?? null ? "max-w-full" : "max-w-3xl",
      true
    ) + " mx-auto rounded-lg group"}">${message.role === "user" ? `${validate_component(UserMessage, "UserMessage").$$render(
      $$result,
      {
        user: $user,
        message,
        isFirstMessage: messageIdx === 0,
        siblings: message.parentId !== null ? history.messages[message.parentId]?.childrenIds ?? [] : Object.values(history.messages).filter((message2) => message2.parentId === null).map((message2) => message2.id) ?? [],
        confirmEditMessage,
        showPreviousMessage,
        showNextMessage,
        copyToClipboard: copyToClipboard2
      },
      {},
      {}
    )}` : `${validate_component(ResponseMessage, "ResponseMessage").$$render(
      $$result,
      {
        message,
        modelfiles: selectedModelfiles,
        siblings: history.messages[message.parentId]?.childrenIds ?? [],
        isLastMessage: messageIdx + 1 === messages.length,
        confirmEditResponseMessage,
        showPreviousMessage,
        showNextMessage,
        rateMessage,
        copyToClipboard: copyToClipboard2,
        continueGeneration,
        regenerateResponse
      },
      {},
      {}
    )}`}</div> </div>`;
  })} ${bottomPadding ? `<div class="mb-10"></div>` : ``}</div>`}`;
});
const ModelSelector = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $models, $$unsubscribe_models;
  let $$unsubscribe_user;
  let $$unsubscribe_settings;
  $$unsubscribe_models = subscribe(models, (value) => $models = value);
  $$unsubscribe_user = subscribe(user, (value) => value);
  $$unsubscribe_settings = subscribe(settings, (value) => value);
  let { selectedModels = [""] } = $$props;
  let { disabled = false } = $$props;
  if ($$props.selectedModels === void 0 && $$bindings.selectedModels && selectedModels !== void 0)
    $$bindings.selectedModels(selectedModels);
  if ($$props.disabled === void 0 && $$bindings.disabled && disabled !== void 0)
    $$bindings.disabled(disabled);
  {
    if (selectedModels.length > 0 && $models.length > 0) {
      selectedModels = selectedModels.map((model) => $models.map((m) => m.id).includes(model) ? model : "");
    }
  }
  $$unsubscribe_models();
  $$unsubscribe_user();
  $$unsubscribe_settings();
  return ` <div class="mb-3 md:p-1 text-left w-full">${each(selectedModels, (selectedModel, selectedModelIdx) => {
    return `<div class="flex flex-wrap-reverse px-2 text-left">${each($models, (model) => {
      return `<div class="${escape(selectedModelIdx > 1 ? "hidden sm:inline-flex" : "", true) + " basis-full sm:basis-1/2 p-[5px] px-1"}" id="models"><button class="${"" + escape(model.name === "hr" ? "hidden" : "", true) + " flex-1 flex justify-between w-full h-full px-4 py-2.5 " + escape(
        selectedModel == model.id ? `bg-blue-900 hover:bg-gray-50 dark:bg-gray-850 dark:hover:bg-gray-800 text-white hover:text-gray-800` : `hover:bg-blue-900 bg-blue-300 hover:text-white dark:bg-gray-850 dark:hover:bg-gray-800`,
        true
      ) + " rounded-2xl transition group"}"><div class="flex flex-col text-center self-center">${model?.name !== "hr" ? `<div class="text-xl text-center uppercase font-medium dark:text-gray-300">${escape(model.name === "llama2:latest" ? "General" : model.name)}</div>` : `<div class="hidden"></div>`}</div> <div class="self-center p-1 rounded-lg text-gray-50 group-hover:text-gray-800 dark:text-gray-850 dark:group-hover:text-gray-100 transition" data-svelte-h="svelte-qf8uca"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M8 14a.75.75 0 0 1-.75-.75V4.56L4.03 7.78a.75.75 0 0 1-1.06-1.06l4.5-4.5a.75.75 0 0 1 1.06 0l4.5 4.5a.75.75 0 0 1-1.06 1.06L8.75 4.56v8.69A.75.75 0 0 1 8 14Z" clip-rule="evenodd"></path></svg> </div></button> </div>`;
    })} </div>`;
  })}</div> <div class="text-left mt-1.5 text-xs text-gray-500"><button data-svelte-h="svelte-zaj6p4">Set as default</button></div>`;
});
const Navbar = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $WEBUI_NAME, $$unsubscribe_WEBUI_NAME;
  let $settings, $$unsubscribe_settings;
  $$unsubscribe_WEBUI_NAME = subscribe(WEBUI_NAME, (value) => $WEBUI_NAME = value);
  $$unsubscribe_settings = subscribe(settings, (value) => $settings = value);
  let { initNewChat } = $$props;
  let { title = $WEBUI_NAME } = $$props;
  let { shareEnabled = false } = $$props;
  let { tags = [] } = $$props;
  let { addTag } = $$props;
  let { deleteTag } = $$props;
  if ($$props.initNewChat === void 0 && $$bindings.initNewChat && initNewChat !== void 0)
    $$bindings.initNewChat(initNewChat);
  if ($$props.title === void 0 && $$bindings.title && title !== void 0)
    $$bindings.title(title);
  if ($$props.shareEnabled === void 0 && $$bindings.shareEnabled && shareEnabled !== void 0)
    $$bindings.shareEnabled(shareEnabled);
  if ($$props.tags === void 0 && $$bindings.tags && tags !== void 0)
    $$bindings.tags(tags);
  if ($$props.addTag === void 0 && $$bindings.addTag && addTag !== void 0)
    $$bindings.addTag(addTag);
  if ($$props.deleteTag === void 0 && $$bindings.deleteTag && deleteTag !== void 0)
    $$bindings.deleteTag(deleteTag);
  $$unsubscribe_WEBUI_NAME();
  $$unsubscribe_settings();
  return ` <nav id="nav" class="sticky py-2.5 top-0 flex flex-row justify-center bg-white/95 dark:bg-gray-900/90 dark:text-gray-200 backdrop-blur-xl z-30"><div class="${"flex " + escape(
    $settings?.fullScreenMode ?? null ? "max-w-full" : "max-w-3xl",
    true
  ) + " w-full mx-auto px-3"}"><div class="flex items-center w-full max-w-full"><div class="pr-2 self-start"><button id="new-chat-button" class="cursor-pointer p-1.5 flex dark:hover:bg-gray-700 rounded-lg transition" data-svelte-h="svelte-1ws8qt"><div class="m-auto self-center"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path d="M5.433 13.917l1.262-3.155A4 4 0 017.58 9.42l6.92-6.918a2.121 2.121 0 013 3l-6.92 6.918c-.383.383-.84.685-1.343.886l-3.154 1.262a.5.5 0 01-.65-.65z"></path><path d="M3.5 5.75c0-.69.56-1.25 1.25-1.25H10A.75.75 0 0010 3H4.75A2.75 2.75 0 002 5.75v9.5A2.75 2.75 0 004.75 18h9.5A2.75 2.75 0 0017 15.25V10a.75.75 0 00-1.5 0v5.25c0 .69-.56 1.25-1.25 1.25h-9.5c-.69 0-1.25-.56-1.25-1.25v-9.5z"></path></svg></div></button></div> <div class="flex-1 self-center font-medium line-clamp-1"><div>${escape(title != "" ? title : $WEBUI_NAME)}</div></div> <div class="pl-2 self-center flex items-center space-x-2">${shareEnabled ? `${validate_component(Tags, "Tags").$$render($$result, { tags, deleteTag, addTag }, {}, {})} <button class="cursor-pointer p-1.5 flex dark:hover:bg-gray-700 rounded-lg transition border dark:border-gray-600" data-svelte-h="svelte-12srkww"><div class="m-auto self-center"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M15.75 4.5a3 3 0 1 1 .825 2.066l-8.421 4.679a3.002 3.002 0 0 1 0 1.51l8.421 4.679a3 3 0 1 1-.729 1.31l-8.421-4.678a3 3 0 1 1 0-4.132l8.421-4.679a3 3 0 0 1-.096-.755Z" clip-rule="evenodd"></path></svg></div></button>` : ``}</div></div></div></nav>`;
});
export {
  ModelSelector as M,
  Navbar as N,
  generateChatCompletion as a,
  copyToClipboard as b,
  createNewChat as c,
  generateTitle as d,
  addTagById as e,
  getAllChatTags as f,
  getChatList as g,
  deleteTagById as h,
  Messages as i,
  MessageInput as j,
  cancelChatCompletion as k,
  getTagsById as l,
  getChatById as m,
  convertMessagesToHistory as n,
  splitStream as s,
  tick as t,
  updateChatById as u
};
